---
theme: seriph
background: https://cover.sli.dev
class: text-center
info: |
  ## Module 4: Kubernetes introduction

  *Containerization and Orchestration Technologies*
  A course by [Malik Algelly](https://ch.linkedin.com/in/malik-algelly-60a7081b2) and [Hugo Haldi](https://haldih.github.io/) for the CUI at the University of Geneva.
highlighter: shiki
drawings:
  persist: false
transition: slide-left
mdc: true
---

# Module 4: Kubernetes - Introduction

*Containerization and Orchestration Technologies*

A course by [Malik Algelly](https://ch.linkedin.com/in/malik-algelly-60a7081b2) and [Hugo Haldi](https://haldih.github.io/) for the CUI at the University of Geneva.

---

## What is Kubernetes?

Kubernetes is an open-source container orchestration platform. It automates the deployment, scaling, and management of containerized applications.

For instance, Kubernetes can:

- Automatically deploy containers across a cluster of physical or virtual machines.
- Scale the number of containers running based on resource utilization.
- Load balance network traffic across a cluster of containers.
- Orchestrate the storage and retrieval of data associated with containers.
- Manage rollouts and rollbacks of containers.
- Provide a platform for implementing CI/CD workflows.
- And much more!

---

## Why Kubernetes? (1)

Let's say you have a web application that you want to deploy to production. You could deploy the application to a single server, but what happens if that server goes down? Your application will go down with it.

You could deploy the application to multiple servers, but:

- How do you load balance traffic across those servers?
- How do you ensure that the application is always available?
- How do you ensure that the application is always up-to-date?
- How do you ensure that the application is always running at peak performance?

You could write custom scripts to handle all of these tasks, but that would be a lot of work.

---

## Why Kubernetes? (2)

Docker compose is somehow an **orchestration tool**, but it is limited to a single host.

A Kubernetes **cluster** is a group of servers that run Kubernetes. Each server in the cluster is called a **node**. A node may be a physical machine or a virtual machine. Each node contains a set of resources managed by Kubernetes.

A node can be a **master node** or a **worker node**. A master node is responsible for managing the cluster, but can also be a worker node. A worker node is responsible for running containers.

An interface, called _Ingress_, is used to load balance traffic across the nodes in the cluster. The Ingress interface can be configured to route traffic to different services based on the URL path or hostname.

---

## How Kubernetes Works (1)

Kubernetes uses a **declarative model** to manage the state of the cluster. You declare the desired state of the cluster in a YAML file called a **manifest**. Kubernetes then ensures that the current state of the cluster matches the desired state.

For examples:

- If we have a manifest that specifies that we want to run three instances of a web application, Kubernetes will ensure that three instances of the application are running at all times. If one of the instances goes down or crashes, Kubernetes will automatically restart it, depending on the `restartPolicy` defined in the manifest.
- when creating a **deployment**, you have to specify the image to use. If the image cannot be retrieved, Kubernetes will keep trying to pull the image until it is available.

---

## Kubernetes Core Components

Kubernetes is a stack of technologies that work together to provide a platform for running containerized applications:

- Kubernetes API Server
- Container Runtime
- Kubelet
- Kubernetes Controller Manager
- Kubernetes Scheduler
- etcd
- kube-proxy

There exists multiple distributions of Kubernetes, and the default container runtime may vary depending on the distribution. However, some components like `kubectl` and `kubelet` are implemented by the Kubernetes project itself.

---

![Kubernetes Architecture](/images/k8s_arch_res.png)

---

### Kubernetes API Server

The API server is the central management point for the cluster. It is present on master nodes and exposes an HTTP API that can be used to manage the cluster. It uses the Kubernetes Control Plane to manage the state of the cluster.

The API can be accessed using the kubectl command-line tool or directly using the HTTP API.

For example, you can use the kubectl command-line tool to list the nodes in the cluster:

```bash
$ kubectl get nodes
NAME           STATUS   ROLES    AGE   VERSION
clusterlab-3   Ready    <none>   75d   v1.28.3
clusterlab-4   Ready    <none>   75d   v1.28.3
clusterlab-1   Ready    <none>   76d   v1.28.3
clusterlab-2   Ready    <none>   75d   v1.28.3
```

This command will send an HTTP request to the API server to list the nodes in the cluster.

This API is however protected by authentication and authorization mechanisms.

---

### Container Runtime

The container runtime is the software that is used to run containers and is compliant with the Kubernetes [CRI (Container Runtime Interface)](https://kubernetes.io/docs/concepts/architecture/cri/). Some examples of container runtimes are *containerd*, *CRI-O*, and *cri-dockerd*.

The container runtime is responsible for starting and stopping containers on the node. It is also responsible for managing the lifecycle of containers, such as restarting containers that have crashed. The container runtime is an abstraction layer that uses the underlying operating system (module 1 memories: namespaces, cgroups, ...) to automate the management of containers.

---

### Kubelet

- A vital agent running on each worker node in the Kubernetes cluster.
- Acts as the primary node-level management unit.

**Responsibilities:**
- Manages the lifecycle of containers on the node, from creation to termination.
- Uses the container runtime (Docker, containerd, etc.) for container management.
- Facilitates seamless communication between the Control Plane and the nodes.

**Key Operations:**
- Registers new nodes with the API server, integrating them into the cluster.
- Monitors the node and container health, reporting back to the API server.
- Ensures pods are running as defined in their manifests by constantly syncing the desired state with the actual container state on the node.

---

### Kubernetes Controller Manager

- A central component that manages various controllers on the master nodes.
- Each controller is a watchdog for a specific cluster function.

**Controllers Include:**
- **ReplicaSet Controller**: Ensures the actual number of pod replicas matches the desired state.
- **Node Controller**: Oversees and manages node health, facilitating node replacements or recovery as needed.
- **Endpoints Controller**, **Service Accounts Controller**, and more, each managing a unique aspect of cluster health and configuration.

**Functionality:**
- Constantly monitors the cluster's state via the API server.
- Takes corrective action to orchestrate the cluster's state towards the user-defined goals.
- Balances workload, manages node health, and ensures the persistence of the desired state.

---

### Kubernetes Scheduler

- Critical for assigning pods to the right nodes, considering current infrastructure and workload.
- Balances resource utilization, complies with policies, and respects constraints to optimize performance and reliability.

**Scheduling Criteria:**
- **Resource Requirements**: CPU, memory, and storage needs.
- **Affinity/Anti-affinity**: Places pods based on node/pod labels for optimal placement.
- **Taints and Tolerations**: Ensures pods are scheduled on appropriate nodes.
- **Other Factors**: Custom rules, health of nodes, and more.

**Outcome:**
- Maximizes resource efficiency, enhances cluster stability, and minimizes downtime.
- Ensures a balanced distribution of workloads across the cluster for optimal performance.

---

### etcd

- A highly-available distributed key-value store that serves as the foundational database of Kubernetes.
- Maintains the cluster's state and configuration, ensuring resilience and consistency across the cluster.

**Key Features:**
- **Highly Reliable**: Designed for critical data storage with fault tolerance and strong consistency.
- **Centralized Management**: Stores configurations, states, metadata, and more, acting as the single source of truth for the cluster.

**Uses in Kubernetes:**
- Persistence of Pod and Service configurations.
- Storage of current states of all Kubernetes objects, like Deployments and ReplicaSets.
- Maintaining information about the cluster's topology and node information.

---

### kube-proxy

- Runs on each node, playing a pivotal role in the Kubernetes networking model.
- Facilitates network communication to and from Pods, both internally within the cluster and externally for services.

**Responsibilities:**
- **Service Abstraction Implementation**: Implements the Kubernetes Service concept, allowing applications to be exposed to the external world or other components within the cluster.
- **Traffic Routing**: Directs incoming or internal traffic to the correct Pods, based on IP and port number of the Services.

**Mechanisms:**
- Can operate in different modes, including iptables, ipvs, and userspace, for various networking scenarios.
- Maintains network rules on nodes which allow network communication to Pods from network sessions inside or outside of the cluster.

---
layout: two-cols
---

## Master Nodes 

- Global cluster management
- API Server
- Scheduling
- Controller manager
- Etcd
- Redundancy for high availability

::right::

## Worker Nodes

- Workload execution
- Run pods
- Responsible for resource allocation
- Kubelet
- Kube-proxy
- Container runtime

---

## Different distributions of Kubernetes (1)

- [Minikube](https://minikube.sigs.k8s.io/docs/)
    - Can use *containerd*, *CRI-O*, or *docker* as container runtime, default may vary depending on the OS or the version
    - Runs a single-node Kubernetes cluster inside a VM on your laptop
    - Useful for development and testing
- [MicroK8s](https://microk8s.io/)
    - Uses *containerd* as container runtime
    - Runs a node Kubernetes cluster on your laptop, desktop, or VM
    - Easy to install and use
    - Can configure a multi-node cluster

---

## Different distributions of Kubernetes (2)

- [k3s](https://k3s.io/)
  - Uses *containerd* as default container runtime, but can use *docker* or *nvidia container runtime* as well
  - Lightweight Kubernetes distribution
  - Optimized for IoT and edge computing
  - Can run on ARM devices
- [OpenShift](https://www.openshift.com/)
  - Uses *cri-o* as container runtime
    - Master nodes use *cri-o* and worker nodes can use *cri-o* or *docker*
  - Enterprise Kubernetes distribution
  - Provides additional features on top of Kubernetes
  - Can manage multiple Kubernetes clusters

---

## Kubernetes concepts

---

### Workloads

A workload is an application running in a Kubernetes cluster. It can be a single components or a group of components that work together to provide a service, running inside a set of pods.

#### Pods

A pod represents a set of running containers in your cluster. It is the smallest deployable unit in Kubernetes. A pod can contain one or more containers, which are managed as a single unit.

However, pods are ephemeral and can be deleted at any time. To ensure that your application is always available, you need to create a workload resource that manages the pods.

#### Deployments

A deployment is a resource that manages a set of pods. It ensures that the desired number of pods are running at all times. If a pod goes down, the deployment will automatically restart it.

---

### Basic Kubernetes deployment

To deploy an application to Kubernetes, you need at least two resources: a **pod** and a **service**. A pod is a group of one or more containers that are deployed together on a node. A service is a way to expose an application running in a pod to the outside world.

---

### Example of a pod manifest

`deployment.yaml`:

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-deployment      # name of the deployment
spec:
  selector:
    matchLabels:
      app: nginx              # label used to select the pods managed by the deployment
                              # and by the service to select the right pods
  replicas: 2                 # tells deployment to run 2 pods matching the template
  template:                   # configures the pods that the deployment will manage
    metadata:
      labels:
        app: nginx            # label used to select the pods managed by the deployment
    spec:
      containers:             # list of containers to run in the pod
      - name: nginx
        image: nginx:1.14.2
        ports:
        - containerPort: 80   # port of the running application inside the container
```

---

### Corresponding service manifest

`serivce.yaml`:

```yaml
apiVersion: v1
kind: Service
metadata:
  name: hello-world-service   # name of the service
spec:
  selector:
    app: nginx                # label used to select the pods
  ports:
    - protocol: TCP
      port: 80
      targetPort: 80          # port on the pod to forward traffic to
      nodePort: 30081         # listen on this port on the node
  type: NodePort              # NodePort, LoadBalancer, ClusterIP
                              # NodePort: expose the service on a port on the node
                              # ClusterIP: expose the service on a cluster-internal IP
                              # LoadBalancer: expose the service using a cloud provider's load balancer
```

---

Then you can create the deployment and the service using the `kubectl apply` command:

```bash
$ kubectl apply -f deployment.yaml
$ kubectl apply -f service.yaml
```

To see the status of the deployment:

```bash
$ kubectl get deployment
NAME               READY   UP-TO-DATE   AVAILABLE   AGE
nginx-deployment   2/2     2            2           7m

$ kubectl get pod
NAME                                READY   STATUS    RESTARTS   AGE
nginx-deployment-86dcfdf4c6-ql5bk   1/1     Running   0          24s
nginx-deployment-86dcfdf4c6-2znd6   1/1     Running   0          24s

$ kubectl get service
NAME                  TYPE       CLUSTER-IP       EXTERNAL-IP   PORT(S)        AGE
hello-world-service   NodePort   10.152.183.231   <none>        80:30081/TCP   6m21s
```

---

### What just happened?

<p v-click>
The configs are stored in <em>etcd</em>, then <em>kube-scheduler</em> assigns pods to nodes, <em>kubelet</em> on each node pulls the image and starts the pod, <em>kube-proxy</em> creates the network rules to forward traffic to the pod, and the service is exposed on the node.

All these components communicate using the Kubernetes API server.
</p>

---

### Understanding port and targetPort in Kubernetes

Kubernetes services utilize the concepts of port and targetPort to route traffic to pods. These fields play a crucial role in the communication between services and pods within a Kubernetes cluster.

- `port`: The port on which the service is exposed inside the cluster. It is the port where the service listens and receives incoming traffic.
- `targetPort`: The port of the pod to which the service routes traffic. This allows incoming traffic on the service's port to be redirected to the targetPort on the corresponding pods.
- `nodePort`: The port on which the service is exposed on each node in the cluster. It allows external traffic to reach the service from outside the cluster.

This arrangement allows for an effective abstraction between services and pods, facilitating the management and exposure of applications within a Kubernetes cluster.

---
layout: image
image: /images/k8s_networking.svg
backgroundSize: contains
---

---

### Persistent Volumes and Persistent Volume Claims

Previously, we have seen that when using deployments, pods are ephemeral and can be deleted at any time. This means that any data stored in the pod will be lost when the pod is deleted.  

In Kubernetes, managing storage is a critical aspect of deploying and managing applications. Two important concepts in the Kubernetes storage model are Persistent Volumes (PVs) and Persistent Volume Claims (PVCs). They provide a way to use storage resources within the cluster, decoupling the storage configuration from the usage.

---

#### Persistent Volumes (PVs)

A Persistent Volume (PV) is a piece of storage in the cluster that has been provisioned by an administrator or dynamically provisioned using *Storage Classes*. It is a resource in the cluster just like a node is a cluster resource. PVs can be attached to zero, one or more pod(s), but have a lifecycle independent of any pod that uses the PV.

#### Characteristics

- Can be provisioned statically or dynamically.
- Supports various storage backends, including NFS, iSCSI, cloud-based storage, and more.
- Managed at the cluster level, not bound to a single pod.

---

#### Persistent Volume Claims (PVCs)

Pods use PersistentVolumeClaims (PVCs) to request physical storage. A PVC describes the storage requirements of a pod and has a lifecycle independent of the pod. PVCs are used to consume PV resources in a cluster.

That means that a PVC can request a specific size and access mode (e.g., read/write) and can be mounted as read/write by many nodes. 

Once a PVC has been created, the Kubernetes control plane tries to find a PV that satisfies the PVC requirements and binds them together. Then, once a PVC is bound to a PV, that PV is exclusively dedicated to the PVC until it is released from the claim.

#### Characteristics

- Acts as a claim on the storage resources defined by a PV.
- Allows workloads to request specific sizes and access modes (e.g., can be mounted as read/write by many nodes).
- Once a PVC is bound to a PV, that PV is exclusively dedicated to the PVC until it is released from the claim.

---

#### How They Work Together

1. Provisioning: An administrator creates a number of PVs in the cluster. They carry details of the real storage which is available for use by cluster users.

2. Claiming: A user creates a PVC that specifies the storage requirements. The Kubernetes control plane looks for a PV that matches the PVC's requirements and binds them together.

3. Using: Once a PVC is bound to a PV, the PVC's claim is satisfied. The PVC can be used as a volume in a Kubernetes pod. The pod can access the PV's storage.

4. Releasing: When a user is done with their volume, they can delete the PVC objects from the API which allows reclamation of the resource. The underlying physical storage is still managed by the administrator.

#### Benefits

- Decoupling of storage configuration from usage: Developers can request storage without knowing the details of the underlying storage system.

- Lifecycle management of storage: Enables efficient management and automation of storage resources within the Kubernetes environment, including provisioning, snapshotting, and resizing.

---

### Example of a Persistent Volume and a Persistent Volume Claim

The following example comes from the [Kubernetes documentation](https://kubernetes.io/docs/tasks/configure-pod-container/configure-persistent-volume-storage/).

---

#### Persistent Volume

`pv.yaml`:

```yaml
apiVersion: v1
kind: PersistentVolume
metadata:
  name: task-pv-volume
  labels:
    type: local # label used by the PVC to select the PV (optional)
spec:
  capacity:
    storage: 10Gi
  accessModes:
    - ReadWriteOnce # ReadWriteOnce: can be mounted as read/write by a single node
                    # ReadWriteMany: can be mounted as read/write by many nodes
                    # ReadOnlyMany: can be mounted as read-only by many nodes
                    # ReadWriteOncePod: can be mounted as read/write by a single pod
  hostPath:
    path: "/mnt/data"
```

```bash
$ kubectl apply -f pv.yaml
$ kubectl get pv task-pv-volume
NAME             CAPACITY   ACCESSMODES   RECLAIMPOLICY   STATUS      CLAIM     STORAGECLASS   REASON    AGE
task-pv-volume   10Gi       RWO           Retain          Available             manual                   4s
```

---

#### Persistent Volume Claim

`pvc.yaml`:

```yaml
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: task-pv-claim # name of the PVC, used by the pod to reference the PVC
spec:
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 3Gi
  selector:
    matchLabels:
      type: local # select the PV based on the label
```

```bash
$ kubectl apply -f pvc.yaml
$ kubectl get pv task-pv-volume
NAME             CAPACITY   ACCESSMODES   RECLAIMPOLICY   STATUS    CLAIM                   STORAGECLASS   REASON    AGE
task-pv-volume   10Gi       RWO           Retain          Bound     default/task-pv-claim   manual                   2m
$ kubectl get pvc task-pv-claim
NAME            STATUS    VOLUME           CAPACITY   ACCESSMODES   STORAGECLASS   AGE
task-pv-claim   Bound     task-pv-volume   10Gi       RWO           manual         30s
```

---

#### Pod using the Persistent Volume Claim

`pod.yaml`:

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: task-pv-pod
spec:
  volumes:
    - name: task-pv-storage # name of the volume, used by the container to reference the volume
      persistentVolumeClaim:
        claimName: task-pv-claim # the PVC to use
  containers:
    - name: task-pv-container
      image: nginx
      ports:
        - containerPort: 80
          name: "http-server"
      volumeMounts:
        - mountPath: "/usr/share/nginx/html"
          name: task-pv-storage # the volume to mount
```

```bash
$ kubectl apply -f pod.yaml
```

---

### Storage Classes

A *StorageClass* provides a way for administrators to describe the "classes" of storage they offer. Different classes might map to quality-of-service levels, backup policies, or other custom storage policies. 

When a user requests a PersistentVolumeClaim (PVC), they can specify a *StorageClass* in the claim. The cluster's storage administrator can then map these classes to the appropriate storage provisioner.

It allows the following:

- Dynamic Provisioning: New PersistentVolumes can be dynamically created based on the Storage Class request, eliminating the need for manual storage provisioning and easing application deployment.
- Abstraction Layer: It abstracts the details of how storage is provided from how it is consumed, offering a more streamlined storage usage model.
- Flexibility: Users can select the storage class that best fits their needs, optimizing cost and performance.

---

#### Provisioner

The main role of a *StorageClass* is to define the provisioner that will be used to create the PersistentVolume. The provisioner is responsible for creating the volume and binding it to the PersistentVolumeClaim.

For example, for a local storage, the provisioner could be `kubernetes.io/no-provisioner`. There is also other provisioners like `AzureFile`, `NFS`, `Ceph`, ... that can be used to create PVs on clouds. See the [Kubernetes documentation](https://kubernetes.io/docs/concepts/storage/storage-classes/#provisioner) for more details.

---

### Example of a Dynamic Provisioning

The following example will show how to create a StorageClass that uses a local storage provisioner to create a PersistentVolume dynamically.

We will create two PVCs that will use the StorageClass to create two PVs.

To do this, we need to install a provisioner that will be used to create the PVs dynamically. In this example, we will use the `microk8s.io/hostpath` provisioner, which is available in MicroK8s. See the [MicroK8s documentation](https://microk8s.io/docs/addon-hostpath-storage) for more details.

---

#### Storage Class

`local-sc.yaml`:

```yaml
apiVersion: storage.k8s.io/v1
kind: StorageClass
metadata:
  name: local-storage                     # name of the StorageClass, used by the PVC to select the StorageClass
provisioner: microk8s.io/hostpath         # provisioner used to create the PV
volumeBindingMode: Immediate              # how the PV should be bound to the PVC
                                          # Immediate: the PV is bound as soon as the PVC is created
```

```bash
$ kubectl apply -f local-sc.yaml
```

---

#### PVCs

<div class="two-columns">

`pvc1.yaml`:

```yaml
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: task-pv-claim1
spec:
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 1Gi
  storageClassName: local-storage
```

`pvc2.yaml`:

```yaml
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: task-pv-claim2
spec:
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 1Gi
  storageClassName: local-storage
```

</div>

---

#### Creating the PVCs

Once the StorageClass is created, we can create the PVCs. The PVCs will use the StorageClass to create the PVs automatically.

```bash
$ kubectl apply -f pvc1.yaml,pvc2.yaml
$ kubectl get pv
NAME           CAPACITY   ACCESS MODES   RECLAIM POLICY   STATUS   CLAIM                    STORAGECLASS    VOLUMEATTRIBUTESCLASS   REASON   AGE
pvc-bdd17...   1Gi        RWO            Delete           Bound    default/task-pv-claim2   local-storage   <unset>                          2m47s
pvc-ff89b...   1Gi        RWO            Delete           Bound    default/task-pv-claim1   local-storage   <unset>                          2m47s

$ kubectl get pvc
NAME             STATUS   VOLUME         CAPACITY   ACCESS MODES   STORAGECLASS    VOLUMEATTRIBUTESCLASS   AGE
task-pv-claim1   Bound    pvc-ff89b...   1Gi        RWO            local-storage   <unset>                 3m51s
task-pv-claim2   Bound    pvc-bdd17...   1Gi        RWO            local-storage   <unset>                 3m51s
```

---

### StatefulSets

A StatefulSet is a Kubernetes resource that is used to manage stateful applications. It is similar to a Deployment, but it provides guarantees about the ordering and uniqueness of the pods it manages.

Each pod in a StatefulSet has a unique identity that is maintained across reschedules. This allows StatefulSets to be used for applications that require stable, unique network identifiers, persistent storage, and ordered deployment and scaling.

Each replica of a StatefulSet has its own persistent storage, which is managed by a PersistentVolumeClaim. This means that each pod in a StatefulSet has its own storage that is independent of the other pods in the StatefulSet.

---

#### StatefulSet Example

`sfs.yaml`:

<div class="two-columns">

```yaml
apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: web
spec:
  selector:
    matchLabels:
      app: nginx # has to match .spec.template.metadata.labels
  serviceName: "nginx"
  replicas: 3 # by default is 1
  template:
    metadata:
      labels:
        app: nginx # has to match .spec.selector.matchLabels
    spec:
      containers: ...
```

```yaml
      - name: nginx
        image: registry.k8s.io/nginx-slim:0.8
        ports:
        - containerPort: 80
          name: web
        volumeMounts:
        - name: www
          mountPath: /usr/share/nginx/html
  volumeClaimTemplates:
  - metadata:
      name: www
    spec:
      accessModes: [ "ReadWriteOnce" ]
      storageClassName: "my-storage-class"
      resources:
        requests:
          storage: 1Gi
```

</div>

---

### ConfigMap

ConfigMaps are key-value pairs used to store configuration settings for your applications. They allow you to decouple environment-specific configuration from your container images, making your applications easily portable.

#### Utilization

- Store configuration data separately from application code.

- Update configuration settings without changing container images or redeploying applications.

- Share configuration data among different parts of the application.

You can create a ConfigMap from literal values or from files.

---

#### Creating a ConfigMap from Literal Values

You can use literal values to create a ConfigMap with a yaml file. Each key-value pair will be stored in the ConfigMap. 

Example:
```yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: mongodb-configmap
data:
    db_host: mongodb-service
    db_port: "27017"
    db_name: mydatabase
```

---

#### Creating a ConfigMap from a file

You can create a ConfigMap from a file using the `kubectl create configmap` command. The file's contents will be stored as the value of a key in the ConfigMap.

With a file named `nginx.conf` containing the following configuration:

```txt
user nginx;
worker_processes auto;

events {
  worker_connections 1024;
}

http {
  server {
    listen 80;
    server_name localhost;

    location / {
      root /usr/share/nginx/html;
      index index.html index.htm;
    }
  }
}

```

---

#### Creating a ConfigMap from a file

We can create a ConfigMap from this file using the following command:

```bash
kubectl create configmap nginx-config --from-file=nginx.conf
```

After creating a ConfigMap from a file, you can view its contents using the `kubectl get configmap` command:

```bash
kubectl get configmap nginx-config -o yaml
```

The output will show the contents of the ConfigMap, including the key-value pair created from the file.

---

#### ConfigMap output

```yaml
apiVersion: v1
kind: ConfigMap
data:
  nginx.conf: |
    user nginx;
    worker_processes auto;

    events {
      worker_connections 1024;
    }

    http {
      server {
        listen 80;
        server_name localhost;

        location / {
          root /usr/share/nginx/html;
          index index.html index.htm;
        }
      }
    }
metadata:
...
```

---

### Secret

Secrets are used to store and manage sensitive information such as passwords, OAuth tokens, and SSH keys. They are similar to ConfigMaps but are specifically intended to keep confidential data secure.

#### Utilization

- Store sensitive data, but they are not encrypted by default !
- Securely manage credentials and other sensitive information.

- Prevent sensitive data from being exposed in plain text.

---

#### Creating a Secret

Secrets can be created from literal values or from files. The data stored in Secrets is base64-encoded.

Example:
```yaml
apiVersion: v1
kind: Secret
metadata:
  name: mongodb-secret
type: Opaque
data:
  username: dXNlcm5hbWU=  # Base64 for "username"
  password: cGFzc3dvcmQ=  # Base64 for "password"
```

---

#### Utilizing Secrets and ConfigMaps in Pods

**Secrets Usage:**
- **As Mounted Data Volumes:** Mount secrets as files in the pod's filesystem to allow applications to access sensitive data directly.
- **As Environment Variables:** Expose secrets to containers within the pod via environment variables for secure transmission of sensitive credentials and configuration details.

**ConfigMaps Usage:**
- **As Environment Variables:** Inject dynamic configuration into containers by exposing ConfigMaps as environment variables, avoiding hardcoding.
- **As Command-Line Arguments:** Pass ConfigMap data as command-line arguments to applications, enhancing flexibility.
- **As Configuration Files:** Mount ConfigMaps as files within a volume, suitable for applications that require reading configurations from files at startup.

---

#### Example: Using a ConfigMap and a Secret in a Deployment

Below is an example of a Kubernetes Deployment that uses both a Secret and a ConfigMap. The Secret is used to pass sensitive data as environment variables, while the ConfigMap is used to configure non-sensitive data.


<div class="two-columns">

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: mongodb
  labels:
    app: mongodb
spec:
  replicas: 1
  selector:
    matchLabels:
      app: mongodb
  template:
    metadata:
      labels:
        app: mongodb
    spec:
      containers:
      - name: mongodb
        image: mongo
```

```yaml
        ports:
        - containerPort: 27017
        env:
        - name: MONGO_INITDB_ROOT_USERNAME
          valueFrom:
            secretKeyRef:
              name: mongodb-secret
              key: username
        - name: MONGO_INITDB_ROOT_PASSWORD
          valueFrom:
            secretKeyRef:
              name: mongodb-secret
              key: password
        - name: DB_HOST
          valueFrom:
            configMapKeyRef:
              name: mongodb-configmap
              key: db_host
```

</div>

---
layout: end
---
