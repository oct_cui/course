---
theme: seriph
background: https://cover.sli.dev
title: "Module 4.5 : Advanced Kubernetes"
info: |
  ## Module 4.5: Advanced Kubernetes

  *Containerization and Orchestration Technologies*
  A course by [Malik Algelly](https://ch.linkedin.com/in/malik-algelly-60a7081b2) and [Hugo Haldi](https://haldih.github.io/) for the CUI at the University of Geneva.
class: text-center
highlighter: shiki
drawings:
  persist: false
transition: slide-left
mdc: true
---

# Module 4.5: Advanced Kubernetes

*Containerization and Orchestration Technologies*

A course by [Malik Algelly](https://ch.linkedin.com/in/malik-algelly-60a7081b2) and [Hugo Haldi](https://haldih.github.io/) for the CUI at the University of Geneva.

---

## Services, Load Balancing, and Networking

**Pods** get their own cluster-wide IP address: no need to create links between pods. Pods can communicate with all other pods without NAT.

**Services** let you expose an application running in Pods to be reachable from outside the cluster.

**Ingress** provides extra functionality for exposing HTTP applications, websites and APIs.

**Gateway API** is a Kubernetes add-on that make network services available by using an extensible, role-oriented, protocol-aware configuration mechanism.

---

### Services

A _Service_ in Kubernetes is a method for exposing a network application running in a set of Pods.

- Don't need to modify existing application to use an unfamiliar service discovery
- If Deployment is used to manage the Pods, it can create and destroy Pods dynamically. You might not know how many Pods are running at any given time, or even what their names are.
- A Service can be used to give a single IP address and DNS name to a set of Pods.

Each Service object defines a logical set of endpoints that are available to be accessed.

The target Pods are selected using a label selector. Kubernetes offers ways to place a network port or load balancer between you application and the backend Pods.

---

#### Discovering Services

Two primary ways to discover a Service:

- **Environment Variables**: variables `${SVCNAME}_SERVICE_HOST` and `${SVCNAME}_SERVICE_PORT` are set for each active Service. Need to create the Service before the Pod.
- **DNS** (preffered way): Kubernetes offers a DNS cluster service to provide a DNS record for each active Service. If a Service named `my-service` in the namespace `my-ns`, the control plane and the DNS Service will create a DNS record for `my-service.my-ns`. Pods in the same namespace can find the Service at `my-service` (`my-service.my-ns` will also work) and Pods in other namespaces can find it at `my-service.my-ns`.

---

### Ingress

_Ingress_ is an API object that manages external access to services in a cluster, typically HTTP. Ingress can provide load balancing, SSL termination, and name-based virtual hosting.

An _Ingress Controller_ is responsible for fulfilling the Ingress, usually with a load balancer, and can be implemented in many ways.

An Ingress only provides HTTP and HTTPS routing. Exposing other services typically uses a service of type `NodePort` or `LoadBalancer`.

---

#### Ingress Resource

Minimal Ingress resource example:

```yaml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: minimal-ingress
  annotations:
    nginx.ingress.kubernetes.io/rewrite-target: /
spec:
  ingressClassName: nginx-example
  rules:
  - http:
      paths:
      - path: /testpath
        pathType: Prefix
        backend:
          service:
            name: test
            port:
              number: 80
```

---

#### Ingress

The name of an Ingress object must be a valid DNS subdomain name:

- no longer than 253 characters
- only lowercase alphanumeric characters, '-' or '.' are allowed
- starts and ends with an alphanumeric character

#### Rules

Each HTTP rule contains the following information:

- **Host**: the host name to match. If not specified, the rule applies to all inbound HTTP traffic through the Ingress.
- **Paths**: a list of URL paths that the Ingress should match. Both the host and path must match the content of an incoming request before the load balancer directs traffic to the referenced Service.
- **Backend**: the Service to send the request to. HTTP (and HTTPS) requests that match the host and path of the Ingress will be sent to the listed Service.

---

#### Path Types

Each path in an Ingress is required to have a path type. There are three path types:

- **ImplementationSpecific**: matching is up to the IngressClass.
- **Exact**: matches the URL path exactly and with case sensitivity.
- **Prefix**: matches based on a URL path prefix split by '/'. The rule is matched if the request URL path is the prefix string or if it has additional path segments.

E.g.:

| Path | Request path | Prefix match? | Exact match? |
| ---- | ------------ | ------------- | ------------ |
| /foo | /foo         | Yes           | yes          |
| /foo | /foo/bar     | Yes           | No           |

See the [Ingress documentation](https://kubernetes.io/docs/concepts/services-networking/ingress/#examples) for more examples.

---

#### Hostname wildcards

Hosts can be defined with wildcards. For example, to match all the `bar.com` subdomains, uses the host `*.bar.com`.

| Host      | Host header     | Match? |
| --------- | --------------- | ------ |
| *.foo.com | bar.foo.com     | Yes    |
| *.foo.com | baz.bar.foo.com | No     |
| *.foo.com | foo.com         | No     |

---

### Ingress Controllers

Ingress can be implemented by different controllers. The controllers are referred to by _Ingress Class_ objects. Each Ingress object should specify the Ingress Class it belongs to.

An Ingress class example:

```yaml
apiVersion: networking.k8s.io/v1
kind: IngressClass
metadata:
  name: external-lb
spec:
  controller: example.com/ingress-controller
  parameters:
    apiGroup: k8s.example.com
    kind: IngressParameters
    name: external-lb
```

The `.spec.parameters` field lets you specify the parameters for the controller.

See the [Ingress Controllers documentation](https://kubernetes.io/docs/concepts/services-networking/ingress-controllers/) to learn more about the available controllers.

---

### Ingress relationships

![Ingress relationships](/images/ingress.svg)

---

### Gateway API

The _Ingress_ specification has been frozen and is no longer accepting new features. The _Gateway API_ is a Kubernetes add-on that provides a new way to configure network services.

_Gateway API_ has three main components:

- **GatewayClass**: defines a set of gateways with common configuration and managed by a contreoller that implements the class.
- **Gateway**: represents a load balancer or proxy that routes traffic to a set of backends.
- **HTTPRoute**: defines how HTTP traffic should be routed to a set of backends.

![gateway relationship](/images/gateway-kind-relationships.svg)

---

#### GatewayClass

Gateways can be implemented by different controllers. Here is an example of a Gateway Class:

```yaml
apiVersion: gateway.networking.k8s.io/v1
kind: GatewayClass
metadata:
  name: example-class
spec:
  controllerName: example.com/gateway-controller
```

For a list of available Gateway controllers, see the [Gateway API documentation](https://gateway-api.sigs.k8s.io/implementations/).

---

#### Gateway

The Gateway is designed to represent a piece of network infrastructure that handles incoming and outgoing traffic. Examples include cloud-based load balancers, hardware or software load balancers running within a data center, and in-cluster proxies.

In the context of Kubernetes, a Gateway provides a way to configure how network traffic should be processed before it reaches backend services. It allows cluster operators to decouple routing and management logic from application workloads.

---

Here is an example of a Gateway:

```yaml
apiVersion: gateway.networking.k8s.io/v1
kind: Gateway
metadata:
  name: example-gateway
spec:
  gatewayClassName: example-class
  listeners:
  - name: http
    protocol: HTTP
    port: 80
```

The listener define the endpoints where the Gateway listens for traffic. Each listener specifies a protocol and port number, and optionally, TLS configuration and hostname details for routing.

We can also specifies the network addresses that the Gateway should listen on. If not specified, the Gateway controller is responsible for assigning an address. Can be configured to listen on specific IP addresses, hostnames, or other methods depending on the implementation.

---

#### HTTPRoute

The _HTTPRoute_ object defines rules for routing HTTP traffic to backend services based on specific characteristics of the HTTP request, such as hostnames, path prefixes, headers, and more.

It facilitates fine-grained traffic management, enabling application developers and cluster operators to control the flow of HTTP traffic within the cluster and optimize it for different use cases such as canary releases, A/B testing, and blue-green deployments.

---

Here is an example of an HTTPRoute:

```yaml
apiVersion: gateway.networking.k8s.io/v1
kind: HTTPRoute
metadata:
  name: example-httproute
spec:
  parentRefs:
  - name: example-gateway
  hostnames:
  - "www.example.com"
  rules:
  - matches:
    - path:
        type: PathPrefix
        value: /login
    backendRefs:
    - name: example-svc
      port: 8080
```

---

- **Parent References:** specifies the Gateways that an HTTPRoute is attached to, defining a relationship where the Gateway acts as an - entry point for the route.
- **Hostnames** defines which hostnames this route applies to. This is used for matching the Host header of incoming HTTP requests.
- **Rules** comprises the core functionality of the route, detailing the conditions under which specific actions are taken. Each rule can include one or more matches and actions.
  - **Match Conditions**: Specify criteria like path prefix, headers, query parameters, etc., that must be met for the rule to be applied.
  - **Actions**: Define what happens when a match is found, typically involving forwarding the traffic to one or more backend services (referred to as backendRefs).

---

#### Gateway Request Flow

![gateway request flow](/images/gateway-request-flow.svg)

---

## Introduction to Horizontal Pod Autoscaler (HPA)

The Horizontal Pod Autoscaler (HPA) automatically scales the number of pods in a _Deployment_, _ReplicaSet_, or _StatefulSet_ based on observed CPU utilization or other select metrics provided by custom metrics APIs.

### Key Features

- **Automatic Scaling**: Adjusts the number of pods dynamically based on real-time usage metrics.
- **Performance Management**: Helps maintain optimal performance and resource efficiency.
- **Cost Optimization**: Scales resources up or down to match demand, reducing costs.
- **Custom Metrics**: Supports custom metrics for more granular control over scaling decisions.

---

### How Does HPA Work?

HPA collects metrics from monitored resources using the [Metrics-Server](https://kubernetes.io/docs/tasks/debug/debug-cluster/resource-metrics-pipeline/) in your Kubernetes cluster. It then adjusts the number of pods proportionally based on its analysis of the CPU load or any given resource.

### Process Flow

1. **Monitor**: Observes the current resource usage against the target you set.
2. **Analyze**: Determines if the load requires scaling action.
3. **Adjust**: Scales the number of pods up or down accordingly.

The Metrics-Server is not configured by default in most of the Kubernetes distributions. You need to install it to enable HPA functionality.

---

### HPA Diagram

![HPA Diagram](/images/k8s_HPA.png)

---

### HPA Diagram

This diagram illustrates the data and operation flow involved in the Horizontal Pod Autoscaler (HPA) process:

- **cAdvisor**: Integrated into the kubelet, this tool gathers, accumulates, and exposes metrics related to container usage.
- **kubelet**: Provides access to resource metrics via the kubelet's API endpoints `/metrics/resource` and `/stats`.
- **Node-Level Resource Metrics**: Offered by the kubelet, this API allows for the retrieval of summarized resource usage statistics per node, which can be found at the `/metrics/resource` endpoint.
- **Metrics Server**: A cluster addon that aggregates resource metrics collected from each kubelet. It feeds the Metrics API, which is utilized by the HPA and VPA, as well as the `kubectl top` command. The Metrics Server serves as a reference example of how to implement the Metrics API.
- **Metrics API**: An API in Kubernetes that facilitates the retrieval of CPU and memory metrics, essential for the autoscaling of workloads. To enable this functionality in your cluster, an API extension server providing the Metrics API is required.

---

### HPA Basic Example

Let’s create a basic HPA that targets CPU utilization for a deployment named `hpa-example`.

The example come from the [Kubernetes documentation](https://kubernetes.io/docs/tasks/run-application/horizontal-pod-autoscale-walkthrough/). It uses a simple php-apache server. To create the deployment and service, run the following commands:

```bash
kubectl apply -f https://k8s.io/examples/application/php-apache.yaml
```

Then, we can create the HPA with the following command:

```bash
kubectl autoscale deployment php-apache --cpu-percent=50 --min=1 --max=10
```
And check the HPA status with:

```bash
kubectl get hpa
NAME         REFERENCE                     TARGET    MINPODS   MAXPODS   REPLICAS   AGE
php-apache   Deployment/php-apache/scale   0% / 50%  1         10        1          18s
```

---

### HPA Basic Example (cont.)

In order to generate load on the server, we can use the following command:

```bash
kubectl run -i --tty load-generator --rm --image=busybox:1.28 --restart=Never -- /bin/sh -c "while sleep 0.01; do wget -q -O- http://php-apache; done"
```

This command creates a pod named `load-generator` that generates a continuous load on the `php-apache` service. The `-i` and `-tty` flags are used to keep the pod running interactively.

---

### HPA Basic Example (cont.)

Now we can watch the HPA in action by running the following command:

```bash
kubectl get hpa php-apache --watch
```

This command lets you watch in real-time as the HPA adjusts the pod count in response to the load.

```bash
NAME         REFERENCE               TARGETS   MINPODS   MAXPODS   REPLICAS   AGE
php-apache   Deployment/php-apache   0%/50%     1         10        1          32s
php-apache   Deployment/php-apache   170%/50%   1         10        1          1m1s
php-apache   Deployment/php-apache   250%/50%   1         10        4          1m42s
php-apache   Deployment/php-apache   65%/50%    1         10        5          2m12s
```

---

### HPA Configuration

<div class="two-columns">

We can also create an HPA using a YAML manifest file. Here is an example of an HPA configuration targeting CPU utilization:

This configuration instructs Kubernetes to keep the CPU usage of `myapp-deployment` at about 50%. It scales between 1 and 10 replicas.

```yaml
apiVersion: autoscaling/v2
kind: HorizontalPodAutoscaler
metadata:
  name: myapp-hpa
spec:
  scaleTargetRef:
    apiVersion: apps/v1
    kind: Deployment
    name: myapp-deployment
  minReplicas: 1
  maxReplicas: 10
  metrics:
    - type: Resource
    resource:
        name: cpu
        target:
            type: Utilization
            averageUtilization: 50

```

</div>

---

### HPA Container Metrics

<div class="two-columns">

We can also use ContainerResource type metrics to scale based on container resource usage.

This configuration targets the `application` container within the `myapp-deployment` deployment. It scales based on the CPU utilization of the `application` container, aiming for 60% utilization.

```yaml
apiVersion: autoscaling/v2
kind: HorizontalPodAutoscaler
metadata:
  name: myapp-hpa
spec:
  scaleTargetRef:
    apiVersion: apps/v1
    kind: Deployment
    name: myapp-deployment
  minReplicas: 1
  maxReplicas: 10
  metrics:
    - type: ContainerResource
    containerResource:
        name: cpu
        container: application
        target:
            type: Utilization
            averageUtilization: 60
```

</div>

---

### Best Practices for Using HPA

To make the most of Horizontal Pod Autoscaler (HPA), consider the following best practices:

- **Set Realistic Targets**: Define CPU and memory requests for all pods to allow HPA to make effective scaling decisions.
- **Use Custom Metrics**: Beyond CPU and memory, consider using custom metrics that better reflect the workload characteristics and demands.
- **Monitor and Adjust**: Regularly review HPA performance and adjust thresholds and limits to align with changing conditions and insights.

---

## Introduction to Vertical Pod Autoscaler (VPA)

The Vertical Pod Autoscaler (VPA) automatically adjusts the CPU and memory allocations for containers in your pods. It analyzes historical and current usage to optimize resource allocations, improving performance and efficiency.

### Key Features

- **Automated Resource Allocation**: Dynamically adjusts container resources in a pod.
- **Performance Optimization**: Ensures pods have resources they need without manual tuning.
- **Cost-Efficient**: Reduces waste by providing just enough resources to run applications smoothly.
- **Works with Historical Data**: Makes decisions based on observed usage over time, not just real-time metrics.

---

### How Does VPA Work?

VPA operates by monitoring resource usage over time and adjusting resource limits and requests for pod containers.

### Operation Flow

1. **Monitor**: Tracks resource usage over time.
2. **Analyze**: Uses historical data to predict future resource requirements.
3. **Recommend**: Suggests resource limits and requests for containers.
4. **Apply**: Automatically updates pods with new resource limits and requests.

---

### Manual scaling

Here's an example of how you can manually set resource limits and requests for a container in a pod.

<div class="two-columns">

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: myapp-deployment
spec:
  replicas: 1
  selector:
    matchLabels:
      app: myapp
  template:
    metadata:
      labels:
        app: myapp
    spec:
```

```yaml
      containers:
      - name: myapp-container
        image: nginx
        resources:
          limits:
            memory: "200Mi"
            cpu: "700m"
          requests:
            memory: "200Mi"
            cpu: "700m"

```

</div>

This configuration sets memory and CPU limits and requests for the `myapp-container` in the `myapp-deployment` deployment. The limits define the maximum resources the container can use, while requests specify the minimum resources required for the container to run.

---

### VPA Configuration

Configure a Vertical Pod Autoscaler (VPA) to automatically adjust the resource allocations of pods based on their usage.

#### VPA Manifest Example

```yaml
apiVersion: autoscaling.k8s.io/v1
kind: VerticalPodAutoscaler
metadata:
  name: myapp-vpa
spec:
  targetRef:
    apiVersion: apps/v1
    kind: Deployment
    name: myapp-deployment
  updatePolicy:
    updateMode: Auto
```

This VPA configuration targets a Deployment named `myapp-deployment`. The `updateMode: Auto` setting allows the VPA to automatically adjust the pod's resource levels without manual intervention.

---

### VPA Update Modes

Understanding the different update modes of VPA can help you better manage how resources are automatically adjusted.

#### Update Modes Explained

- **Auto**: VPA updates the pod's resources automatically as it deems necessary.
- **Off**: VPA does not update the pod's resources but provides recommended values.
- **Initial**: VPA only applies resource settings at pod startup. Subsequent changes must be manually applied.
- **Recreate**: VPA can recreate the pods when updating resources to apply changes that require a restart.

---

### Monitoring VPA Recommendations

You can monitor VPA recommendations and actions to ensure that your pods are optimized for resource usage.

#### Check VPA Recommendations

```bash
kubectl describe vpa myapp-vpa
```

This command provides detailed information about the VPA recommendations for the `myapp-vpa` Vertical Pod Autoscaler.

---

## HPAs vs. VPAs

In Kubernetes, the rescaling of pods, either horizontally through the Horizontal Pod Autoscaler (HPA) or vertically through the Vertical Pod Autoscaler (VPA), relies on various metrics to make informed decisions about when and how to adjust resource allocations.

---

### Metrics Used by HPA

1. **CPU Utilization**: This is the most common metric used by HPA. It scales the number of pod replicas based on the average CPU utilization of the pods.
2. **Memory Utilization**: Though less common than CPU, memory can also be a metric for HPA when configured with custom metrics support.
3. **Custom Metrics**: Beyond CPU and memory, HPA can use custom metrics provided by Kubernetes metrics APIs. These could be anything from queue length to request per second, depending on what is exposed by your applications.

### Metrics Used by VPA

1. **CPU and Memory Requests**: VPA adjusts the CPU and memory requests based on the observed usage of the pods, aiming to match the actual usage patterns more closely than the often manually set requests.
2. **Historical Usage Data**: VPA uses historical resource usage data to recommend resource allocation. This helps in dynamically adjusting the resources of pods that might have changing resource requirements over time.

---

### Operation of Autoscalers

- **HPA** works by monitoring the specified metrics and then scaling the number of pod replicas within a deployment or stateful set to meet the desired target metrics. It adjusts the pod count to maintain the average target utilization specified across all pods.
- **VPA**, on the other hand, adjusts the resource requests for the containers in a pod, rather than changing the number of pods. It updates the requests to ensure pods have enough resources and are not over-provisioned, which can save costs and improve efficiency.

Both autoscalers require the Kubernetes Metrics Server to be installed and running in your cluster. This server collects and aggregates metrics like CPU and memory usage from the kubelets on each node and then exposes these metrics to the autoscalers for decision-making.

Each autoscaler is suitable for different scenarios, depending on whether the workload's demand changes in terms of pod count (horizontal scaling) or resource per pod (vertical scaling). It's crucial to understand the characteristics of your applications to choose the appropriate scaling strategy or combination of strategies.

---

## Generate a valid certificate for a Kubernetes Ingress

To generate a valid certificate for a Kubernetes Ingress, you can use the `cert-manager` tool. `cert-manager` is a Kubernetes add-on that automates the management and issuance of TLS certificates from various certificate authorities.

`cert-manager` can be used to generate certificates from Let's Encrypt, Venafi, HashiCorp Vault, and other certificate authorities. It integrates with Kubernetes Ingress resources to automatically provision and renew certificates for your services.

---
layout: image
image: /images/cert-manager-overview.svg
---

---

### Issuer and ClusterIssuer

`cert-manager` defines two new resources to manage certificate issuance:

- **Issuer**: An Issuer is a namespaced resource that represents a certificate authority (CA) that can issue certificates. Issuers are scoped to a single namespace.
- **ClusterIssuer**: A ClusterIssuer is a cluster-wide resource that represents a CA that can issue certificates. ClusterIssuers are not namespaced and can be used to issue certificates across all namespaces.

An example of an `Issuer` type is `CA`. A simple `CA` `Issuer` is as follow:

```yaml
apiVersion: cert-manager.io/v1
kind: Issuer
metadata:
  name: ca-issuer
  namespace: mesh-system
spec:
  ca:
    secretName: ca-key-pair
```

---

### Example using ACME Issuer

An example of an `ACME` `ClusterIssuer` is as follow:

```yaml
apiVersion: cert-manager.io/v1
kind: ClusterIssuer
metadata:
  name: letsencrypt-staging
spec:
  acme:
    # You must replace this email address with your own.
    # Let's Encrypt will use this to contact you about expiring
    # certificates, and issues related to your account.
    email: user@example.com
    server: https://acme-staging-v02.api.letsencrypt.org/directory
    privateKeySecretRef:
      # Secret resource that will be used to store the account's private key.
      name: example-issuer-account-key
    # Add a single challenge solver, HTTP01 using nginx
    solvers:
    - http01:
        ingress:
          ingressClassName: nginx
```

---

### Configuring Ingress with TLS

To configure an Ingress with TLS, we can add annotations to the Ingress resource. These annotations will facilitate creating the `Certificate` by `cert-manager`.

Here is an example of an Ingress resource with TLS configuration:

```yaml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  annotations:
    # add an annotation indicating the issuer to use.
    cert-manager.io/cluster-issuer: nameOfClusterIssuer
  name: myIngress
  namespace: myIngress
spec:
  rules:
  - host: example.com
    ...
  tls: # < placing a host in the TLS config will determine what ends up in the cert's subjectAltNames
  - hosts:
    - example.com
    secretName: myingress-cert # < cert-manager will store the created certificate in this secret.
```
