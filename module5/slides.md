---
theme: seriph
background: https://cover.sli.dev
title: "Module 5: Helm"
info: |
  ## Module 5: Highways to Helm

  *Containerization and Orchestration Technologies*
  A course by [Malik Algelly](https://ch.linkedin.com/in/malik-algelly-60a7081b2) and [Hugo Haldi](https://haldih.github.io/) for the CUI at the University of Geneva.
class: text-center
highlighter: shiki
drawings:
  persist: false
transition: slide-left
mdc: true
---

# Module 5: Highways to Helm

*Containerization and Orchestration Technologies*

A course by [Malik Algelly](https://ch.linkedin.com/in/malik-algelly-60a7081b2) and [Hugo Haldi](https://haldih.github.io/) for the CUI at the University of Geneva.

---

## What is Helm?

Helm is a package manager for Kubernetes. It allows you to define, install, and manage Kubernetes applications. It simplifies the process of deploying and managing applications on Kubernetes clusters.

Helm can be thought of as the `apt` or `yum` of Kubernetes.

It uses a packaging format called charts, which are a collection of files that describe a related set of Kubernetes resources.

Each chart is a bundle of YAML files that describe a related set of Kubernetes resources.

---

## Why Helm?

As the number of Kubernetes resources grows, managing them becomes more complex. Helm simplifies this process by providing a way to define, install, and manage Kubernetes applications:

- **Streamlined Deployments:** Helm packages applications into "charts" which are easy to deploy.
- **Versioning:** Manage different versions of your application configurations effortlessly.
- **Reusability:** Share and reuse application configurations with Helm Charts.

---

## Key Concepts

The Helm ecosystem consists of the following key concepts:

- **Charts:** Packages of pre-configured Kubernetes resources.
- **Templates:** Helm uses Go templates to generate Kubernetes manifests.
- **Repositories:** Share and discover Helm Charts through repositories.

---

## Helm Workflow

The typical Helm workflow consists of the following steps:

1. **Install Helm:** Get Helm up and running on your machine.
2. **Create or Use a Chart:** Define or find a Helm Chart for your application.
3. **Customize Values:** Modify the default configuration parameters.
4. **Install or Upgrade:** Deploy your application to Kubernetes using Helm.
5. **Enjoy!** Your application is now running on Kubernetes.

You may also need to add repositories where Helm can find charts. For example, the bitnami repository contains many popular applications. To add the bitnami repository, run:

```bash
helm repo add bitnami https://charts.bitnami.com/bitnami
```

---

## Helm Ecosystem

Helm is part of a larger ecosystem of tools that help you manage Kubernetes applications:

- **Helm Hub:** A central repository of Helm Charts.
- **Helm repositories:** Share and discover Helm Charts.
- **Helm plugins:** Extend Helm's functionality with plugins.
- **Helmfile:** A declarative way to manage Helm releases.

---

## Helm charts

A Helm chart is a collection of files that describe a related set of Kubernetes resources. It contains the following components:

- **Chart.yaml:** Metadata about the chart.
- **values.yaml:** Default configuration values.
- **templates:** Kubernetes manifests that are rendered using Go templates.
- **charts:** Dependencies on other charts.
- **helpers:** Helper templates used in the chart.

The chart can be packaged into a `.tgz` file and distributed to others or published to a Helm repository:

```bash
helm package mychart
helm push mychart myrepo
```

This command will create a `mychart-{version}.tgz` file that can be installed using Helm.

---

## Getting Started with Helm

To get started with Helm, you need to install the Helm client on your machine. Check the Helm documentation for instructions on how to install Helm on your platform.

Once you have Helm installed, you can start using Helm to deploy applications to Kubernetes. Helm provides a set of commands that you can use to manage Helm Charts and deploy applications.

---

## Start a Helm Chart

To start a new Helm Chart, you can use the `helm create` command:

```bash
helm create mychart
```

This will create a new directory called `mychart` with the following structure:

```
mychart/
  Chart.yaml
  values.yaml
  charts/
  templates/
  ...
```

You can then customize the chart by modifying the `values.yaml` file and the templates in the `templates` directory.

---

## Install a Helm Chart

To install a Helm Chart, you can use the `helm install` command:

```bash
helm install myrelease mychart
```

This will deploy the Helm Chart to your Kubernetes cluster with the release name `myrelease`.

You can also specify custom values for the chart using the `--set` flag:

```bash
helm install myrelease mychart --set key1=value1,key2=value2
```

---

## Start writing Helm templates

Helm uses Go templates to generate Kubernetes manifests. It will render the templates in the `templates` directory and generate the Kubernetes manifests that will be deployed to your cluster. The `values.yaml` file contains the values that will be fed into the templates.

The key point of using Helm is to create reusable templates that can be used to deploy applications to Kubernetes. This allows you to define your application once and deploy it multiple times with different configurations.

Let's take a look at an example Helm template:

```yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: {{ .Release.Name }}-configmap
data:
  myvalue: "Hello World"
```

Each template directive is enclosed in double curly braces `{{ }}`. The `{{ .Release.Name }}` directive will be replaced with the release name when the template is rendered.


---

## Deep Dive into Helm templating engine

The template directive `{{ .Release.Name }}` injects the release name into the template. Directives can be read as follows:

- `.`: The top-most namespace for this scope. It is the context in which the template is evaluated.
- `.Release`: The release field inside the top-most namespace.
- `.Release.Name`: The name field inside the release field.

So we could read `.Release.Name` as "start at the top namespace, find the `Release` object, then look inside of it for an object named `Name`".

The built-in objects in Helm templates can be found in the [Helm documentation](https://helm.sh/docs/chart_template_guide/builtin_objects/).

---

## Template Functions and Pipelines

We've seen how to access values in the template using the `.` operator. Helm also provides a set of built-in functions that you can use to manipulate values in the template.

For example, it is a best practice to quote strings injected from the `.Values` object. This can be done using the `quote` function in the template directive:

```yaml
myvalue: {{ quote .Values.myvalue }}
```

The `quote` function will ensure that the value is properly quoted when rendered in the template.

In this case, the generated manifest will look like this:

```yaml
myvalue: "Hello World"
```

if we set `myvalue: Hello World` in the `values.yaml` file, or use the `--set myvalue="Hello World"` flag.

Template functions follow the syntax `{{ function arg1 arg2 ... argN }}`. In the above example, `quote` is the function and `.Values.myvalue` is the argument.

---

### Pipelines

Helm enables you to chain functions together using pipelines. A pipeline is a sequence of commands separated by the pipe character `|`. The output of one command is passed as input to the next command. It comes from the Unix shell concept of piping.

<div class="two-columns">

Here is an example of a pipeline in a Helm template:

```yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: {{ .Release.Name }}-configmap
data:
  myvalue: "Hello World"
  drink: {{ .Values.favorite.drink | quote }}
  food: {{ .Values.favorite.food | upper | quote }}
```

In this example, the `favorite.drink` value is quoted using the `quote` function, and the `favorite.food` value is uppercased using the `upper` function before being quoted.

When pipelining arguments, the result of the first evaluation is sent as the last argument to the function. `{{ .Values.favorite.food | repeat 5  | quote }}` will repeat the value of `favorite.food` 5 times before quoting it.

</div>

---

## Helm and Go Templates

A common function used in Helm templates is the `default` function. This function allows you to set a default value if the value is not provided in the `values.yaml` file. For example:

```yaml
drink: {{ .Values.favorite.drink | default "tea" | quote }}
```

In this example, if the `favorite.drink` value is not set in the `values.yaml` file, the default value "tea" will be used.

For a complete list of template functions, refer to the [Helm documentation](https://helm.sh/docs/chart_template_guide/function_list/). See also the [Go template documentation](https://golang.org/pkg/text/template/).

---

## Flow Control in Helm Templates

Control structures in Helm templates allow you to conditionally render parts of the template based on the values read. Helm provides the following control structures:

- `if` and `else` for conditional blocks
- `with` to specify a scope
- `range` to iterate over a list, "for each"-style loop

You have access to some actions for declaring and using named templates segments:

- `define` to declare a named template
- `template` to import a named template
- `block` to define a block of content that can be overridden

---

### Conditional Blocks

The basic structure of an `if` block in a Helm template is as follows:

```yaml
{{ if PIPELINE }}
  # Do something
{{ else if OTHER_PIPELINE }}
  # Do something else
{{ else }}
  # Do default case
{{ end }}
```

A pipeline is considered as `false` is the value is:

- A boolean `false`
- A numeric zero
- An empty string
- A nil (empty or null)
- An empty collection (array, slice, map, or channel)

---

## Comprehensive Example

Go and Helm embed some conditional functions that can be used in the templates. Some of them are `eq`, `ne`, `lt`, `gt`, `and`, `or`, ...

Let's add some conditional logic to our Helm template. We'll add another setting if the drink is coffee:

```yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: {{ .Release.Name }}-configmap
data:
  myvalue: "Hello World"
  drink: {{ .Values.favorite.drink | default "tea" | quote }}
  food: {{ .Values.favorite.food | upper | quote }}
  {{ if eq .Values.favorite.drink "coffee" }}mug: "true"{{ end }}
```

---

## Whitespaces

Since YAML is whitespace-sensitive, Helm templates have to be careful with the indentation. If we retake the previous example, changing the indentation:

```yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: {{ .Release.Name }}-configmap
data:
  myvalue: "Hello World"
  drink: {{ .Values.favorite.drink | default "tea" | quote }}
  food: {{ .Values.favorite.food | upper | quote }}
  {{ if eq .Values.favorite.drink "coffee" }}
    mug: "true"
  {{ end }}
```

If we try to render the template, we will get the following error:

```bash
$ helm install --dry-run --debug ./mychart
SERVER: "localhost:44134"
CHART PATH: /Users/mattbutcher/Code/Go/src/helm.sh/helm/_scratch/mychart
Error: YAML parse error on mychart/templates/configmap.yaml: error converting YAML to JSON: yaml: line 9: did not find expected key
```

---

### What happened?

The generated YAML file is not valid because of the indentation:

```yaml
# Source: mychart/templates/configmap.yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: eyewitness-elk-configmap
data:
  myvalue: "Hello World"
  drink: "coffee"
  food: "PIZZA"
    mug: "true" # <-- This line is not correctly indented
```

---

### Fixing the indentation

Let's simply out-dent the `mug` line:

```yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: {{ .Release.Name }}-configmap
data:
  myvalue: "Hello World"
  drink: {{ .Values.favorite.drink | default "tea" | quote }}
  food: {{ .Values.favorite.food | upper | quote }}
  {{ if eq .Values.favorite.drink "coffee" }}
  mug: "true"
  {{ end }}
```

---

### Result

Now the generated YAML file is valid, but still looks a little bit weird:

```yaml
# Source: mychart/templates/configmap.yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: telling-chimp-configmap
data:
  myvalue: "Hello World"
  drink: "coffee"
  food: "PIZZA"
                  # <-- Why is there so much space here?
  mug: "true"
```

Why? <v-click>Whe the template renders, the content inside of `{{` and `}}` will be removed, but it leaves the remaining whitespace exactly as is.</v-click>

---

## Access and modify scope

Recall that `.` refers to the current scope. So `.Values` refers to the `Values` object in the current scope. If you want to access a value in a different scope, you can use the `with` action:

```yaml
{{ with PIPELINE }}
  # restricted scope
{{ end }}
```

`with` set the current scope to a particular object. For example, if you want to access a value in the `Values` object, you can use `with`:

```yaml
{{ with .Values.favorite }}
  drink: {{ .drink | default "tea" | quote }}
  food: {{ .food | upper | quote }}
{{ end }}
```

Note that the `if` from the previous example is now unnecessary, as the `with` block will only be executed if the value of `PIPELINE` is not empty.

---

### Root scope

```yaml
{{- with .Values.favorite }}
  drink: {{ .drink | default "tea" | quote }}
  food: {{ .food | upper | quote }}
  release: {{ .Release.Name }}
{{- end }}
```

This template will fail because the `Release` object is not in the `favorite` object. To access the `Release` object, you need to end the `with` block, or use `$` to access the root scope:

```yaml
{{- with .Values.favorite }}
  drink: {{ .drink | default "tea" | quote }}
  food: {{ .food | upper | quote }}
  release: {{ $.Release.Name }}
{{- end }}
```

---

## Iterating over lists

Helm provides the `range` action to iterate over a list. The basic structure of a `range` block is as follows:

```yaml
{{- range PIPELINE }}
  # Do something
{{- end }}
```

The `range` action will iterate over the list provided by the pipeline and execute the block for each item in the list, setting the current scope to the item.

Let see an example with the following `values.yaml`:

```yaml
favorite:
  drink: coffee
  food: pizza
pizzaToppings:
  - mushrooms
  - cheese
  - peppers
  - onions
```

---

### Example

<div class="two-columns">

`configmap.yaml`:

```yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: {{ .Release.Name }}-configmap
data:
  myvalue: "Hello World"
  {{- with .Values.favorite }}
  drink: {{ .drink | default "tea" | quote }}
  food: {{ .food | upper | quote }}
  {{- end }}
  toppings: |-
    {{- range .Values.pizzaToppings }}
    - {{ . | title | quote }}
    {{- end }}    
```

Generated YAML:

```yaml
# Source: mychart/templates/configmap.yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: edgy-dragonfly-configmap
data:
  myvalue: "Hello World"
  drink: "coffee"
  food: "PIZZA"
  toppings: |-
    - "Mushrooms"
    - "Cheese"
    - "Peppers"
    - "Onions"  
```

</div>

Note: the `|-` marker in YAML takes a multi-line string. This is because the data in ConfigMaps `data` is composed of key-value pairs, where both the key and the value are simple strings.

---

## Variables

Helm templates also allow you to define variables. They can be used to simplify code, and make better use of `with` and `range` blocks.

To define a variable, use the Go assignation operator `:=`. From the example before:

```yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: {{ .Release.Name }}-configmap
data:
  myvalue: "Hello World"
  {{- $relname := .Release.Name -}}
  {{- with .Values.favorite }}
  drink: {{ .drink | default "tea" | quote }}
  food: {{ .food | upper | quote }}
  release: {{ $relname }}
  {{- end }}
```

---

### Variables in `range` loops

In `range` loops, they can be used on list-like objects to capture both the index and the value:

```yaml
toppings : |-
  {{- range $index, $topping := .Values.pizzaToppings }}
    {{ $index }}: {{ $topping }}
  {{- end }}
```

It will produces:

```yaml
toppings : |-
  0: mushrooms
  1: cheese
  2: peppers
  3: onions
```

---

### Using variables for key-value pairs

For data structures that have both a key and a value, you can use `range` to get both:

<div class="two-columns">

```yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: {{ .Release.Name }}-configmap
data:
  myvalue: "Hello World"
  {{- range $key, $val := .Values.favorite }}
  {{ $key }}: {{ $val | quote }}
  {{- end }}
```

```yaml
# Source: mychart/templates/configmap.yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: eager-rabbit-configmap
data:
  myvalue: "Hello World"
  drink: "coffee"
  food: "pizza"
```

</div>

---

### Example

You can use `$` inside a `range` block to access the root scope:

```yaml
{{- range .Values.tlsSecrets }}
apiVersion: v1
kind: Secret
metadata:
  name: {{ .name }}
  labels:
    # Many helm templates would use `.` below, but that will not work,
    # however `$` will work here
    app.kubernetes.io/name: {{ template "fullname" $ }}
    # I cannot reference .Chart.Name, but I can do $.Chart.Name
    helm.sh/chart: "{{ $.Chart.Name }}-{{ $.Chart.Version }}"
    app.kubernetes.io/instance: "{{ $.Release.Name }}"
    # Value from appVersion in Chart.yaml
    app.kubernetes.io/version: "{{ $.Chart.AppVersion }}"
    app.kubernetes.io/managed-by: "{{ $.Release.Service }}"
type: kubernetes.io/tls
data:
  tls.crt: {{ .certificate }}
  tls.key: {{ .key }}
---
{{- end }}
```

---

## Partials and `_` files

Helm allows you to define named mebedded templates, that can be accessed by name elsewhere.

Every file in `templates/` are treated as if they contain Kubernetes manifests, except for files that start with an underscore `_` (and `NOTES.txt`). The failes whise name begins with `_` are assumed to not have a manifest inside and therefore are not rendered, but can be included in other templates.

When creating a template chart, it creates a `_helpers.tpl` file that contains some useful functions and partials.

---

### Declaring a partial with `define`

To define a partial, use the `define` action:

```yaml
{{- define "MY.NAME" }}
  # body of template here
{{- end }}
```

To define a template to encapsulate a Kubernetes block of labels:

```yaml
{{- define "mychart.labels" }}
  labels:
    generator: helm
    date: {{ now | htmlDate }}
{{- end }}
```

Now you can use this partial in other templates:

<div class="two-columns">

```yaml
metadata:
  name: {{ .Release.Name }}-configmap
  {{- include "mychart.labels" }}
```

```yaml
metadata:
  name: running-panda-configmap
  labels:
    generator: helm
    date: 2016-11-02
```

</div>

---
layout: end
---
