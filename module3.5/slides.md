---
# try also 'default' to start simple
theme: seriph
# random image from a curated Unsplash collection by Anthony
# like them? see https://unsplash.com/collections/94734566/slidev
background: https://cover.sli.dev
# some information about your slides, markdown enabled
title: Welcome to Slidev
info: |
  ## Module 3.5: Design dockerizable applications

  *Containerization and Orchestration Technologies*
  A course by [Malik Algelly](https://ch.linkedin.com/in/malik-algelly-60a7081b2) and [Hugo Haldi](https://haldih.github.io/) for the CUI at the University of Geneva.
class: text-center
# https://sli.dev/custom/highlighters.html
highlighter: shiki
# https://sli.dev/guide/drawing
drawings:
  persist: false
# slide transition: https://sli.dev/guide/animations#slide-transitions
transition: slide-left
# enable MDC Syntax: https://sli.dev/guide/syntax#mdc-syntax
mdc: true
---

# Module 3.5: Design dockerizable applications

*Containerization and Orchestration Technologies*

A course by [Malik Algelly](https://ch.linkedin.com/in/malik-algelly-60a7081b2) and [Hugo Haldi](https://haldih.github.io/) for the CUI at the University of Geneva.

---

## Good practices when designing applications

Until now, we have seen how to use Docker to run applications and dockerize them. But when we design applications, we should also think about how to make them easy to dockerize.

In this intermediate course, we will see how to design applications so that they can be easily dockerized.

---

## Clearly define the application's dependencies

When designing an application, we should clearly define its dependencies. This includes the programming language, the libraries, the database, etc.

For example, if we are designing a Python application, we should define the Python version and the libraries that the application uses.

A good way to do this is to use virtual environments. This way, we can clearly define the dependencies and avoid conflicts with other applications. It makes simple to create a `requirements.txt` file (`pip freeze > requirements.txt`) that can be used to install the dependencies in a Docker container.

---

## Hardcoded configuration vs environment variables

When designing an application, we should avoid hardcoding configuration. Instead, we should use environment variables.

For example, instead of hardcoding the database URL in the code, we should use an environment variable:

```python
DATABASE_URL = 'postgresql://user:password@localhost:5432/mydatabase'
```

```python
import os

DATABASE_URL = os.environ['DATABASE_URL']
```

Then we can set the environment variable when we run the application:

```bash
DATABASE_URL=postgresql://user:password@localhost:5432/mydatabase python app.py
```

Or when we run the application in a Docker container:

```bash
docker run -e DATABASE_URL=postgresql://user:password@db:5432/mydatabase myapp
```

---

### But why?

Hardcoding configuration makes the application less flexible. If we want to change any configuration, we have to change the code and rebuild the image.

For instance, Docker uses internal DNS to resolve container names. This means that if we run a database in a container, we can access it from another container using the container name.

This is very convenient, but it means that we need to use environment variables to configure the application, otherwise we would have to hardcode the container name in the code, which would involve the rebuilding of the image every time we change the database container name.

Example using compose:

```yaml
services:
  app:
    image: myapp
    environment:
      - DATABASE_URL=postgresql://user:password@db:5432/mydatabase
  db:
    image: postgres
```

---

## Configuration files

Another way to configure an application is to use configuration files. To do so, we should mount the path to the configuration file as a volume when we run the container.

For example, if we want to create a GitLab runner, we can mount the `/etc/gitlab-runner` directory as a volume:

```bash
docker run --rm -it -v /srv/gitlab-runner/config:/etc/gitlab-runner gitlab/gitlab-runner register
```

This way, we can change the configuration and keep it persistent (i.e. it will not be lost when the container is removed).

---

## Persistent data

When designing an application, we should also think about where the data will be stored. We should avoid storing data inside the container, as it will be lost when the container is removed. Thus, we should use volumes to store the data outside the container (i.e. using `VOLUME` instruction in Dockerfile). This way, the data will be persistent.

Imagine we have an application storing its data and code in `/app`:

```
/app/
  app.py
  user1/
  user2/
  ...
```

If we want to bind a volume to this directory, we would override the `/app` directory with the volume, preventing the application from accessing its code.

---

## Persistent data (cont.)

Instead, we should use a specific directory for the data, for example `/app/data`. This way, we can bind a volume to `/app/data` without overriding the code:

```
/app/
  app.py
  data/
    user1/
    user2/
    ...
```

---

## Logging

When designing an application, we should also think about logging. We should think about where the logs will be stored and how they will be collected.

When using containers, we should log to the standard output and standard error. This way, we can use the Docker logging driver to collect the logs.

---

## Single responsibility principle

Avoid having an application that does too many things. Instead, split the application into smaller services, each with a single responsibility.

Also, avoid having multiple applications in a single container. Instead, use multiple containers.

For example, if you have a database and database administration tool, you should use two containers, one for the database and one for the administration tool:

```yaml
services:
  db:
    image: postgres
  admin:
    image: admin
    environment:
      - DATABASE_URL=postgresql://user:password@db:5432/mydatabase
```

---

## In memory storage

If you need to store volatile data, you should use an in-memory storage. For example, if you need to store a cache, you should use a cache server like Redis.

When using containers, you should use a separate container for the in-memory storage. This way, you can scale the application and the storage independently.

Example using compose:

```yaml
services:
  app:
    image: myapp
    environment:
      - CACHE_URL=redis://cache:6379
  cache:
    image: redis
    volumes:
      - type: tmpfs
        target: /data
        tmpfs:
          size: 100m
```