---
theme: seriph
background: https://cover.sli.dev
title: "Module 6: CI/CD integration with Kubernetes"
info: |
  ## Module 6: CI/CD integration with Kubernetes

  *Containerization and Orchestration Technologies*
  A course by [Malik Algelly](https://ch.linkedin.com/in/malik-algelly-60a7081b2) and [Hugo Haldi](https://haldih.github.io/) for the CUI at the University of Geneva.
class: text-center
highlighter: shiki
drawings:
  persist: false
transition: slide-left
mdc: true
---

# Module 6: CI/CD integration with Kubernetes

*Containerization and Orchestration Technologies*

A course by [Malik Algelly](https://ch.linkedin.com/in/malik-algelly-60a7081b2) and [Hugo Haldi](https://haldih.github.io/) for the CUI at the University of Geneva.

---

![CI/CD Pipeline](/images/cicd_pipeline.png)

---

## What is CI/CD?

**Continuous Integration (CI)** and **Continuous Deployment (CD)** are practices that help automate the process of building, testing, and deploying applications.

- **Continuous Integration (CI)**: Developers integrate their code changes into a shared repository multiple times a day. Each integration is verified by an automated build and test process to detect errors early.
- **Continuous Deployment (CD)**: Changes that pass the CI process are automatically deployed to production.

A pipeline is a set of automated steps that define the CI/CD process. It typically includes building the application, running tests, and deploying the application to a target environment.

---

## GitLab CI/CD

There is a wide range of CI/CD tools available, such as GitHub Actions, Jenkins, CircleCI, Travis CI, etc. In this module, we will focus on GitLab CI/CD.

GitLab CI/CD is a powerful tool that allows developers to define pipelines using a `.gitlab-ci.yml` file in the root of their repository. The pipeline is defined using a set of stages and jobs that run in parallel or sequentially.

GitLab CI/CD uses **runners** to execute the pipeline jobs. A runner is a lightweight agent that runs the jobs defined in the pipeline.

---

### Example `.gitlab-ci.yml` file

Here is an example of a `.gitlab-ci.yml` file that defines a simple pipeline with two stages: `build` and `test`.

```yaml
stages:
  - build
  - test

build:
  stage: build
  script:
    - echo "Building the application..."

test:
  stage: test
  script:
    - echo "Running tests..."
```

---

## GitLab runners

The jobs defined in `.gitlab-ci.yml` are defined as scripts that run on a GitLab runner. The runner will execute the script in a specific environment, such as a Docker container, or a Kubernetes pod.

When an event triggers a pipeline, GitLab will select an available runner to execute the pipeline jobs. The runner will download the repository, execute the jobs, and report the results back to GitLab.

An event can be a push to a branch, a merge request, a tag, or a scheduled pipeline.

---

### Runner executor types

GitLab runners can be configured to use different executor types:

- **Shell**: The job script is executed directly on the runner machine.
- **Docker**: The job script is executed inside a Docker container.
- **Kubernetes**: The job script is executed inside a Kubernetes pod.
- **SSH**: The job script is executed on a remote machine using SSH.
- ...

See the [GitLab documentation](https://docs.gitlab.com/runner/executors/) for a complete list of executor types.

In this module, we will focus on the **Docker** and **Kubernetes** executor types.

---

### Docker executor

When using the Docker executor, the runner will try to find the Docker socket on the host machine to start a Docker container. The job script will be executed inside the container using the image specified in the `.gitlab-ci.yml` file.

So the GitLab runner pilots the host machine's Docker daemon to start and delete containers.

However, the GitLab runner often runs in a container itself, which means it cannot access the host machine's Docker socket directly. To solve this issue, the Docker executor can be configured to use Docker-in-Docker (DinD) mode.

---

### Run GitLab Runner in a container

As said before, the GitLab runner has to access the Docker socket on the host machine to start containers. A common way to achieve this is to bind-mount the Docker socket into the GitLab runner container.

Here is an example of how to run a GitLab runner in a container:

```bash
docker run -d --name gitlab-runner --restart always \
  -v /var/run/docker.sock:/var/run/docker.sock \
  -v /srv/gitlab-runner/config:/etc/gitlab-runner \
  gitlab/gitlab-runner:latest
```

This command will start a GitLab runner container that has access to the host machine's Docker socket, and stores its configuration in `/srv/gitlab-runner/config` on the host machine.

This way, when the Runner receives a job, it can start a Docker container to execute the job script.

---

### Security considerations

Running a GitLab runner in a container with access to the Docker socket can be a security risk. The runner can start containers on the host machine, which can lead to privilege escalation attacks. It is important to secure the host machine and the GitLab runner container.

You may need to configure the runner to use the host machine's Docker socket, for instance, to run tests that require Docker inside the job script, or to deploy some application inside the pipeline. To do this, you can use the `docker:dind` image in the job script and configure the runner to use Docker-in-Docker mode. See [how to use Docker-in-Docker](https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#use-docker-in-docker).

Be aware of the security implications of running jobs having access to the Docker socket:

- Any job can start a container on the host machine, which can lead to privilege escalation attacks, data leaks, etc.
- Any project member with access to the `.gitlab-ci.yml` file can run arbitrary code on the host machine.
- ...

See [who has access to runners in GitLab](https://docs.gitlab.com/runner/#who-has-access-to-runners-in-the-gitlab-ui) for more information.

---

### Kubernetes executor

The Kubernetes executor allows GitLab to use a Kubernetes cluster to run the pipeline jobs. The runner will use the Kubernetes cluster API to create a Kubernetes pod for each job, and execute the job script inside the pod.

The Kubernetes executor divides the build into multiple steps:

1. Prepare: The runner creates a Kubernetes pod with the necessary resources to run the job.
2. Pre-build: Clone, restore cache, and download artifacts. Run on a special container as part of the pod.
3. Build: Run the job script inside the main container of the pod.
4. Post-build: Create cache, upload artifacts, and cleanup. Also uses the special container.

---
layout: center
---

#### Sequence diagram

![Kubernetes executor sequence diagram](/images/k8s_executor_sd.png){ width=80% }

---

### Installing the GitLab runner on Kubernetes

To install the GitLab runner on a Kubernetes cluster, you can use the official GitLab runner Helm chart. The Helm chart will deploy the GitLab runner as a Kubernetes deployment, and configure it to use the Kubernetes executor.

When the GitLab runner is deployed on the Kubernetes cluster, it will use the auto-discovery feature to access the Kubernetes API and create pods to run the pipeline jobs.

A Helm chart is available to deploy the GitLab runner on Kubernetes:

```bash
helm repo add gitlab https://charts.gitlab.io
helm install --namespace <namespace> gitlab-runner -f <values.yaml> gitlab/gitlab-runner
```

Where `<namespace>` is the Kubernetes namespace where the GitLab runner will be deployed, and `<values.yaml>` is a file containing the configuration values for the GitLab runner.

---

### Configuration available in the Helm chart

The following configuration keys must be specified in the `values.yaml` file for the GitLab runner to work correctly:

- `gitlabUrl`: The URL of the GitLab instance (e.g. `https://gitlab.unige.ch`).
- `rbac: { create: true }`: Create RBAC rules for the runner to create pods to run jobs in.
- `runnerToken`: The registration token of the runner (when [registering the runner with GitLab](https://docs.gitlab.com/ee/ci/runners/runners_scope.html#create-an-instance-runner-with-a-runner-authentication-token)).

See [`values.yaml` reference](https://gitlab.com/gitlab-org/charts/gitlab-runner/blob/main/values.yaml) for a complete list of configuration options.

---

## GitLab Agent

As we seen before, the Kubernetes executor allows GitLab to use a Kubernetes cluster to run the pipeline jobs in a Kubernetes pod. However, what if we want to deploy an application directly on the cluster? The pod created by the runner has no access to the Kubernetes API, so it cannot deploy resources on the cluster.

To solve this issue, GitLab introduced the concept of **GitLab Agent**. The GitLab Agent is a service that runs on the Kubernetes cluster and can deploy, manage and monitor cloud-native applications.

Before GitLab agent was introduced, GitLab was using a certificate-based integration to deploy resources on the Kubernetes cluster. This required the runner to have access to the Kubernetes API, which was a security risk. This solution was deprecated in GitLab 13.0 in favor of the GitLab Agent.

---

### Installing the GitLab Agent

To install the GitLab Agent on a Kubernetes cluster, you must first configure the repository to use the GitLab Agent. This can be done directly in the GitLab UI, in your project settings `Operate > Kubernetes clusters` section.

Once the agent is configured, the GitLab UI will provide you with the command to install the agent on the Kubernetes cluster. The command will create a Kubernetes deployment for the agent, and configure it to connect to the GitLab instance.

Alternatively, you can use the GitLab Agent Helm chart to install the agent on the Kubernetes cluster:

```bash
helm repo add gitlab https://charts.gitlab.io
helm repo update
helm upgrade --install test gitlab/gitlab-agent \
    --namespace gitlab-agent-test \
    --create-namespace \
    --set image.tag=<current agentk version> \
    --set config.token=<your_token> \
    --set config.kasAddress=<address_to_GitLab_KAS_instance>
```

---

### Testing the GitLab Agent

Once you have installed the GitLab Agent on the Kubernetes cluster, you can test it by deploying an application using a CI/CD pipeline.

Here is an example of a `.gitlab-ci.yml` file that mimic the deployment of an application on the Kubernetes cluster using the GitLab Agent:

```yaml
deploy:
  image:
    name: bitnami/kubectl:latest
    entrypoint: ['']
  script:
    - kubectl config get-contexts
    - kubectl config use-context path/to/agent/project:agent-name
    - kubectl get pods
```

The pod created by the runner will have access to the Kubernetes API through the GitLab Agent, and will be able to deploy resources on the cluster.

