---
theme: default
background: https://cover.sli.dev
class: text-center
highlighter: shiki
lineNumbers: false
info: |
  ## Module 1: Low-level containerization

  *Containerization and Orchestration Technologies*
  A course by [Malik Algelly](https://ch.linkedin.com/in/malik-algelly-60a7081b2) and [Hugo Haldi](https://haldih.github.io/) for the CUI at the University of Geneva.
drawings:
  persist: false
transition: slide-left
title: Containerization and Orchestration Technologies
mdc: true
---

# Module 1: Low-level containerization

*Containerization and Orchestration Technologies*

A course by [Malik Algelly](https://ch.linkedin.com/in/malik-algelly-60a7081b2) and [Hugo Haldi](https://haldih.github.io/) for the CUI at the University of Geneva.

---

## What is Containerization?

---

### What is an OS?

An operating system (OS) is a software that acts as an intermediary between computer hardware and the computer user

It includes the following components:

- **Kernel**: The core of the OS
- **System libraries**: Software libraries that provide the basic functions of the OS
- **System utilities**: Software that provides the basic functions of the OS

They can be accessed using an **Application programming interface (API)**, a set of functions and procedures that allow the creation of applications that access the features or data of an OS, application, or other service (e.g. Linux system calls, Libc (C standard library), etc.)

---

### Virtual Machines

- Virtual Machines (VMs) are an abstraction of physical hardware turning one computer into many computers
- VMs involves a [hypervisor](https://www.vmware.com/topics/glossary/content/hypervisor.html.html) and each VM contains a full copy of an operating system, one or more apps, necessary binaries and libraries

Full stack of virtualized hardware:

- **Physical hardware**: The actual hardware
- **Kernel**: The core of the OS
- **Hypervisor**: A software that creates and runs VMs, emulating the physical hardware
- **Guest OS**: A full copy of an operating system (kernel, system libraries, system utilities, etc.)

---
layout: center
---

### Virtualisation stack

![Virtualisation stack](/images/virt_stack.webp)

---

### VMs vs Containers

#### Containers

- Containers are an **abstraction** at the **app layer** that packages code and dependencies together
- Containerization is a **lightweight alternative** to full machine virtualization
- Containers are **isolated** from one another and bundle their own software, libraries and configuration files
- They can communicate with each other through well-defined channels

Full stack of containerized software:

- **Physical hardware**: The actual hardware
- **Kernel**: The core of the OS
- **Container Engine**: A software that creates and runs containers, emulating the physical hardware
- **Container**: A package that contains an application and its dependencies (system libraries, system utilities, etc.)

---
layout: center
---

### Containerization stack

<img src="/images/cont_stack.png" alt="Containerization stack" width="600"/>

---
layout: image
image: /images/virt_vs_cont_stack.png
backgroundSize: contain
---

---

## Why Containerization?

- **Portability**: Containers are highly portable
  - A piece of software that runs on a laptop can run on any other machine that runs Linux
- **Efficiency**: Containers are lightweight
  - Contrary to VMs, containers do not require a hypervisor nor a guest OS
  - Can be efficiently created and destroyed
- **Isolation**: Containers are isolated from each other
  - Containers can only access resources that are explicitly allowed
- **Security**: Containers are secure
  - Since containers are isolated from each other, a compromised container cannot compromise other containers

---

## Why Containerization? (cont.)

- **Scalability**: Containers are scalable
  - Containers can be easily scaled horizontally
  - E.g. if a high load is detected, more containers can be created to handle the load
- **Productivity**: Containers increase developer productivity
  - Containers allow developers to focus on writing code rather than setting up the environment
- **Cost**: Containers reduce costs
  - Containers allow you to run more workloads on the same hardware
- **CI/CD**: Containers are a key enabler of CI/CD
  - Containers allow you to build once and deploy anywhere

---

## How containerization works?

Kernel provides features that allow to isolate processes from each other:

- **Namespaces**: Isolate system resources
- **Cgroups**: Limit the amount of resources a process or a group of processes can use
- **Seccomp**: Fine-grained control over which system calls a process can make (capabilities)

---

## Namespaces

Definition:

> Namespaces are a feature of the Linux kernel that partitions kernel resources such that one set of processes sees one set of resources while another set of processes sees a different set of resources.
>
> -- <cite>Wikipedia</cite>

In other words, *namespaces* allow you to create an **isolated environment** for a process or a group of processes. The goal is to prevent processes from seeing resources that they are not allowed to see. For example, one can put a process in a namespace where it can only see a specific network interface.

Changes made to a resource in a namespace are only visible to processes in that namespace.

---

## Types of Namespaces

- **UTS**: Hostname and domain name

- **PID**: Process IDs

- **Network**: Network devices, stacks, ports, etc.

- **Mount**: Mount points

- **User**: User and group IDs

---

### UTS Namespace

- UTS stands for *Unix Timesharing System*
- UTS namespace allows each container to have its own **hostname** and **domain name**
- Changes made to the hostname in a UTS namespace are **only visible** to processes in **that namespace**

- A child process **inherits** the UTS namespace of its parent process
- Hostname and domain name are inherited from the parent namespace when a new namespace is created

---
layout: two-cols
---

#### In practice

<div style="margin-right: 20px">

```bash
# 1. Get the hostname
$ hostname
ms-7917

# 4. Check that the hostname
# has not changed in the initial
# UTS namespace
$ hostname
ms-7917

# 5. Change the hostname in the
# initial UTS namespace
$ hostname yggdrasil
```

</div>

::right::

#### <br/> <!-- It works, so it's not dumb -->

<div style="margin-left: 20px">

```bash
# 2. Create a new UTS namespace
$ unshare -u
$ hostname
ms-7917

# 3. change the hostname in
# the new UTS namespace
$ hostname thor
$ hostname
thor

# 6. Check that the hostname
# has not changed in the new
# UTS namespace
$ hostname
thor
```

</div>

---

### Mount Namespace

What is a *mount point*?

- A mount point is a **directory** in the filesystem from which the content of a **storage device** is made available to the user
- E.g. when you plug in a USB drive, by default on Ubuntu the content of the USB drive which device file is `/dev/sdb1` is made available to the user in the `/media/$USER/$NAME_OF_THE_USB_DRIVE` directory
- A mount point source can be a **device file**, a **directory** or a **remote filesystem**

---
layout: two-cols
---

### Mounting a device

In this example, the **main** storage device is mounted at the mount point `/`, which serves as the *root filesystem*-the starting point of the entire directory structure.

We then mount another storage device at `/mnt/volume1`, meaning that its files become accessible within the `/mnt/volume1` directory.

This allows the operating system and users to navigate the second device's contents as if they were part of the main filesystem.

::right::

![Device mount](/images/device_mountpoint.png)

---
layout: two-cols
---

#### Example: directory bind mount 

<div style="text-align: justify; margin-right: 30px;">

In this example, we are mounting the directory `./test1` onto `./test2`. Since the source is a directory, this type of mount is called a *bind mount* (using the `--bind` option). Essentially, we are linking `./test1` to `./test2`, making them refer to the same underlying content.  

After the bind mount operation, `./test2` effectively acts as an alias for `./test1`. Any changes made to files within `./test1` will immediately appear in `./test2`, and vice versa, since both paths now access the same data.

</div>

::right::

```console
$ ls -R ./
./:
test1  test2
./test1:
file1
./test2:

$ sudo mount --bind ./test1 ./test2

$ ls -R ./
./:
test1  test2
./test1:
file1
./test2:
file1

$ touch ./test2/file2

$ ls -R ./
./:
test1  test2
./test1:
file1  file2
./test2:
file1  file2
```

---

### Mount Namespace (cont.)

What is a *mount namespace*?

- Mount namespace allows each **container** to have its own **set of mount points**
- Mount namespace is **hierarchical**, i.e. a child mount namespace copies the mount points of its parent mount namespace
- Changes made to the mount points in a mount namespace are **only visible** to processes in **that namespace**

---
layout: two-cols
---

#### Process A

<div style="margin-right: 20px">

```shell
# 1. List the files in the current directory
$ ls -R ./
./:
test1  test2
./test1:
file1
./test2:

# 3. Create a new mount namespace
$ sudo unshare -m

# 4. Create the bind mount
$ mount --bind ./test1 ./test2

# 5. Check that the bind mount has been created
$ ls -R ./
./:
test1  test2
./test1:
file1
./test2:
file1
```

</div>

::right::

<div style="margin-left: 20px">

#### Process B

```shell
# 2. List the files in the current directory
$ ls -R ./
./:
test1  test2
./test1:
file1
./test2:

# 6. Check that the bind mount is not visible
# in other mount namespaces
$ ls -R ./
./:
test1  test2
./test1:
file1
./test2:
```

</div>

---

### PID Namespace

- PID stands for *Process IDentifier*

- PID namespace allows each **group of processes** to have its own **set of PIDs**

- Two process can have the same PID in different PID namespaces, however they are not the same process

- The **first process** in a new PID namespace becomes **PID 1** (*init* process)

- PID namespaces are **hierarchical**, thus they can be seen as **parent-child relationships**

- A process inside the parent PID namespace can see the processes in the child PID namespace **but not vice versa**

---

### PID Namespace (cont.)

![PID Namespaces](/images/pid_namespaces.svg){width=65%}

*Schema from [this conference by Michael Kerrisk](https://youtu.be/0kJPa-1FuoI)*

---
layout: two-cols
---

#### In practice

<div style="margin-right: 50px; text-align: justify;">

First, we create a new PID namespace with the `--fork` option. This means that the new PID namespace will be a child of the current PID namespace.

Since we cannot change the PID namespace of a running process, we need to create a new process in the new PID namespace. This is done with the `--fork` option.

We can see that the PID of the new process is 1, which means that it is the *init* process of the new PID namespace.

However, because of the `/proc` filesystem, the new process can see the processes in the parent PID namespace. We can see that using the `ps` command.

</div>

::right::

<div style="margin-right: 20px;">

#### <br/>

```shell
$ unshare --pid --fork
$ echo $$
1
$ ps
    PID TTY          TIME CMD
  18165 pts/2    00:00:00 sudo
  18166 pts/2    00:00:00 unshare
  18167 pts/2    00:00:00 bash
  18280 pts/2    00:00:00 ps
```

</div>

---

### PID Namespace (cont.)

<p style="font-size: 40px; text-align: center; margin-top: 15%; line-height: 4rem;"><!-- So BEAUTIFUL !-->What should we do if we want to isolate the new process from the parent PID namespace?</p>

---
layout: two-cols
---

#### More complete example

<div style="text-align: justify; margin-right: 20px;">

We create both a new PID namespace and a mount namespace. Then, we initiate a process in the newly created PID namespace and mount the `/proc` filesystem in the mount namespace.

We can observe that this process becomes the *init* process of its PID namespace and is only able to see the processes within this specific namespace.

</div>

::right::

#### <br/>

<div style="margin-left: 20px;">

```shell
$ unshare --pid --fork --mount
$ mount -t proc proc /proc
$ ps
    PID TTY          TIME CMD
      1 pts/2    00:00:00 bash
     14 pts/2    00:00:00 ps
```

</div>

---


### Network Namespace


- Network namespace allows each container to have its own network stack (network interfaces, routing tables, iptables rules, etc.)

- One network interface can only be in one network namespace at a time

- We can use virtual network device pair to provide connectivity between network namespaces

- When a namespace is freed, a physical device is moved back to the initial network namespace while a virtual device is destroyed

---
layout: two-cols
---

#### Virtual Network Device Pair

<div style="text-align: justify; margin-right: 20px;">

A virtual network pair device, often referred to as a "veth" pair in the context of Linux networking, consists of two virtual network interfaces that are connected like a tube: what comes in one end goes out the other.

These devices are usually used to connect different network namespaces in Linux, providing a way to establish network communication between them.

</div>

::right::

<div style="margin-left: 20px;">

![Network namespace device pair](/images/netns.svg)

</div>

---

#### In practice

Creation of a veth pair connecting the host and Loki network namespaces

```shell
ip netns add loki
ip link add eth0-l type veth peer name veth-l
ip link set eth0-l netns loki
ip link set veth-l up
ip address add 10.0.0.1/24 dev veth-l
ip netns exec loki ip link set lo up
ip netns exec loki ip link set eth0-l up
ip netns exec loki ip address add 10.0.0.2/24 dev eth0-l
```

---

### User Namespace

- Isolate **users** and **groups identifiers** (UIDs and GIDs) and **capabilities**

- UID and GID can be mapped to other UID and GID **inside the namespace**

- Process can have **unprivileged** UID and GID **outside the namespace** and **privileged** UID and GID **inside the namespace**

- When a new user namespace is created, the **first process** in the namespace has **all capabilities**

---

#### Hierarchy

- User namespaces have a **hierarchical relationship**
  - each user namespace has a **parent user namespace**, except for the initial ("root") user namespace

- User namespaces can have multiple children user namespaces

- A **maximum of 32** user namespaces can be nested

- A process is a member of **exactly one** user namespace

---

### User Namespace (cont.)

- A user namespace can **own other namespaces** (mount, PID, network, etc.)
- Capabilities only apply to the resources that are **member of the namespace owned by the current user namespace**
- E.g. a process having the `CAP_NET_ADMIN` capability can only modify network interface that are member of the namespace owned by the current user namespace

![User Namespaces](/images/user_namespaces.svg){width=70%}

*Schema from [this conference by Michael Kerrisk](https://youtu.be/0kJPa-1FuoI)*

---

### UID and GID mappings

How to map UIDs and GIDs between the parent and child user namespaces?

- Defined by writing to 2 files in the `/proc` filesystem:
  - `/proc/$PID/uid_map`
  - `/proc/$PID/gid_map`

Records written to these files have the following format:

```shell
ID-inside-ns ID-outside-ns length
```

Meaning:

- `ID-inside-ns` and `length` define the range of UIDs or GIDs in the child namespace that are to be mapped
- `ID-outside-ns` defines the starting point of the range of UIDs or GIDs in the parent namespace that are to be mapped

---

### UID and GID mappings (example)

Generally initialized with a single line containing "root mapping":

```shell
$ cat /proc/self/uid_map
0       1000          1
```

This line means that the root user (UID 0) in the child user namespace is mapped to the user with UID 1000 in the parent user namespace.

---

## Capabilities

<!-- Capabilities are a way to limit the amount of system calls a process can make. They are a way to give a process the minimum amount of privileges it needs to run.

This way, even if a container is running as root, it cannot do everything that the root user can do.

We will see how to use capabilities in the next module (Docker). -->

Historically, UNIX systems have divided users into two categories: *normal users* and *privileged users* (root). Privileged users have **full access** to the system, while normal users have **limited access**.

```bash
chown root prog
chmod u+s prog
```

When executed, `prog` will run with the privileges of the owner of the file, which is `root`.

However, it doesn't seem logical to have to run a program wanting to access a small set of privileged resources with full privileges. For example, should a program wanting to open a socket on a port below 1024 have access to the entire system?

Capabilities address this issue. They allow granting a program **limited access** to certain **privileged resources**.

You can find the list of capabilities in the [`man 7 capabilities`](https://man7.org/linux/man-pages/man7/capabilities.7.html)

---

### Few common capabilities

- `CAP_NET_BIND_SERVICE`: Bind a socket to a port below 1024. E.g. if you try to run a web server on port 80, you'll get a permission denied error if you don't have this capability or if you're not root.
- `CAP_SYS_CHROOT`: Change the root directory. With this capability, you won't need root privileges to create a chroot environment.
- `CAP_SYS_ADMIN`: Perform a range of system administration operations. E.g. mounting and unmounting filesystems, setting system time, etc. **Should be avoided if possible**, since a lot of capabilities gives more fine-grained control over the system. It can plausibly be called "the new root".

---

### Deep dive into the capabilities

A process has **five** distinct sets of capabilities, each with a specific role:

- **The Effective set**: the capabilities **currently in effect** for the process, i.e., those that will be used to check permissions. If a process tries to access a resource protected by a capability that is not in the Effective set, **access will be denied**.

- **The Permitted set**: the capabilities that the process **can activate**. A process can activate a capability that is in the Permitted set but not yet in the Effective set by using the `capset()` function. This will have the effect of **moving** the capability from the **Permitted set** to the **Effective set**. This mechanism allows limiting the capabilities of a process during its execution. For example, the *ping* command uses the `CAP_NET_RAW` capability to open a raw socket, then disables this capability.

- **The Bounding set**: the capabilities that **can be added** to the **Permitted set**. This limits the capabilities of a process. If a capability is not in the Bounding set, it can never be added to the Permitted set, and therefore never be activated.

---

### Deep dive into the capabilities (cont.)

- **The Inheritable set**: the capabilities that will be **passed on** through an `exec()`. When a process performs an `exec()`, it **loses all the capabilities** in its **Permitted** and **Effective sets** but keeps those in its Inheritable set. This allows **transmitting capabilities** to a new process. Additionally, as we will see later, executables also possess **sets of capabilities**. Thanks to this, after an `exec()`, a process can be granted additional capabilities in its Permitted and Effective sets.

- **The Ambient set**: when a non-privileged process performs an `exec()`, it **loses all its capabilities** except those in the **Ambient set**. This allows granting capabilities to a non-privileged process.

---

### Inheritable Set in details

The inheritable capabilities of a process define the set of capabilities that this process can potentially pass on to programs it executes. However, for an inheritable capability to be effectively transmitted during an execve(), it must also be present as inheritable in the executed file.

This mechanism allows for fine control over which capabilities can be retained through execution chains without automatically granting all possible capabilities to each new process.

#### Interaction with `execve()`

When a process executes a new program with execve(), the Linux kernel performs a calculation to determine the new process's permitted and effective capability sets based on several factors, including the calling process's inheritable capability set and the capability markers of the executed file.

The inheritable capabilities of the calling process are intersected with the inheritable capabilities marked on the executed file to determine which capabilities can be added to the new process's permitted set.

---

### File Capabilities

*File capabilities* sets are stored in an **extended attribute** of the file system. Writing to this extended attribute **requires** the `CAP_SETFCAP` capability.

The file capabilitiy sets with the thread capability sets are used to **determine the new process's capability sets** after an `execve()`.

There are three file capability sets:

- **Permitted**: These capabilities are **automatically** added to the process's permitted set when the file is executed.
- **Inheritable**: This set is **ANDed** with the **inheritable** set of the **calling process** to determine which capabilities can be passed on to the **new process**.
- **Effective**: This is not a set, but rather just **a single bit**. If the file has this bit set, **all of the permitted capabilities** are **automatically** added to the **effective set** of the process. If not, **none of the permitted capabilities** are added to the effective set.

---

#### Example

Imagine a process executing a script or program without specific capabilities attached and without inheritable capability markers. Even if the parent process has certain capabilities in its permitted or effective set, these capabilities will not automatically be transmitted to the new process created by `execve()`.

Suppose a parent process has the `CAP_NET_BIND_SERVICE` capability in its inheritable set and executes a program whose inheritable capabilities also include `CAP_NET_BIND_SERVICE`.

1. Preparation of the Parent Process:  
   The parent process ensures that `CAP_NET_BIND_SERVICE` is in its inheritable set (and possibly in its bounding set to use it directly).

2. Marking the Executable File:  
   The target executable is marked to have `CAP_NET_BIND_SERVICE` in its set of inheritable capabilities. This can be achieved with `setcap cap_net_bind_service+ei /path/to/program`.

3. Execution with `execve()`:  
   When the parent process executes this program with `execve()`, the new process inherits the `CAP_NET_BIND_SERVICE` capability in its permitted set, provided that the logic of `execve()` and other capability sets (such as the bounding set) allow this transmission.


---

#### In practice

Let's take the example of the `nc` command. This command is used to open a network connection to a specific port. By default, it cannot open a connection to a port below 1024. However, we can give it this capability.

```shell
sudo setcap cap_net_bind_service=ei ./nc # We have copied the `nc` command to the current directory
sudo -E capsh --inh=cap_net_bind_service --user=$USER -- # We start a new shell with the inheritable capability
./nc -l 80 # We can now open a connection to port 80 even if it is below 1024 without being root
```

---

### Ambient Set in details

The ambient capability set is a concept introduced in the Linux kernel starting from version 4.3. It is a mechanism that allows a process to **retain certain capabilities** across `execve()` calls, even when it executes a program that is not privileged. This set complements the existing capability sets (effective, permitted, inheritable, and bounding) and offers greater flexibility in the management of process rights, especially in contexts where inheritable capabilities are **not sufficient**.

#### Functioning of Ambient Capabilities

Before the introduction of ambient capabilities, a process's capabilities could be passed on through an `execve()` only if the executed file had defined capabilities or if the executing process was root. This limited the ability of non-root processes to retain privileges through an `execve()`. Ambient capabilities solve this problem by allowing certain capabilities to be explicitly retained by a non-privileged process through an `execve()`.

A process can add or remove capabilities from its ambient set using the `prctl()` system call. The capabilities in the ambient set are added to the process's permitted and effective capability sets after an `execve()`.

---

#### Restrictions

- Ambient capabilities are subject to restrictions to enhance security. A capability can only be added to the ambient set if it is already present in the process's permitted and inheritable capability sets.
- Executing a program that changes the UID or GID of the process, or a program with file capabilities, clears the ambient capability set.

---

#### Example

Imagine a scenario where a non-root UID process needs to execute a new program while retaining the CAP_NET_BIND_SERVICE capability (which allows binding a socket to ports numbered below 1024).

Without Ambient Capabilities:
- Before Linux 4.3, without ambient capabilities, the process would either need to be executed by root or have CAP_NET_BIND_SERVICE in the file capabilities of the executed program to bind a port below 1024.

With Ambient Capabilities:

1. The parent process adds CAP_NET_BIND_SERVICE to its ambient capability set.
2. The parent process executes execve() to launch a new program.
3. The new program inherits CAP_NET_BIND_SERVICE in its permitted and effective capability sets thanks to the ambient set, even if the program has no file capabilities and the user is not root.

---

#### In practice

For this example, we'll need some C code to illustrate how the *ambient set* works. With this short program [set_ambient](https://github.com/ContainerSolutions/capabilities-blog/blob/master/set_ambient.c), we can add the `CAP_NET_BIND_SERVICE` capability to the ambient set of a new process.

##### Add file privilege

```bash
$ sudo setcap cap_net_bind_service+p set_ambient
$ getcap ./set_ambient
./set_ambient = cap_net_bind_service+p
```

##### Try to execute with bit in ambient set

```bash
$ ./set_ambient /bin/bash
Starting process with CAP_NET_BIND_SERVICE in ambient
$ grep Cap /proc/$BASHPID/status
CapInh: 0000000000000400
CapPrm: 0000000000000400
CapEff: 0000000000000400
CapBnd: 0000003fffffffff
CapAmb: 0000000000000400
$ capsh --decode=0000000000000400
0x0000000000000400=cap_net_bind_service
```

---

### Transformations of capabilities during `execve()`

From `man capabilities(7)`:

```
P'(ambient)     = (file is privileged) ? 0 : P(ambient)

P'(permitted)   = (P(inheritable) & F(inheritable)) |
                  (F(permitted) & P(bounding)) | P'(ambient)

P'(effective)   = F(effective) ? P'(permitted) : P'(ambient)

P'(inheritable) = P(inheritable)    [i.e., unchanged]

P'(bounding)    = P(bounding)       [i.e., unchanged]
```

Where

```
P()    denotes the value of a thread capability set before the execve(2)

P'()   denotes the value of a thread capability set after the execve(2)

F()    denotes a file capability set
```

---

### Particular case with *set-user-ID-root* or *ruid AND euid* are 0

When a process with nonzero UIDs `execve(2)s` a *set-user-ID-root* program that does not have capabilities attached, or when a process whose real and effective UIDs are zero `execve(2)s` a program, the calculation of the process's new permitted  capabilities simplifies to:

```
P'(permitted)   = P(inheritable) | P(bounding)

P'(effective)   = P'(permitted)
```

Thus, in this case the *bounding set* defines the capabilities that a new process will obtain.

This means that we have to be careful to correctly set the bouding set to limit the capabilities a process can have.

---

## Secure Bits

The securebits flags, introduced in Linux kernel version 2.6.26, enhance process capability management, especially limiting superuser (UID 0) capabilities. These flags create environments where privileges are managed solely through capabilities, mitigating automatic UID change effects.

Key flags include:

- `SECBIT_KEEP_CAPS`, which lets processes retain capabilities despite UID changes;
- `SECBIT_NO_SETUID_FIXUP`, preventing kernel adjustments to capability sets during UID transitions; 
- `SECBIT_NOROOT`, stopping automatic capability grants for set-user-ID-root executions or processes with UID 0; 
- and `SECBIT_NO_CAP_AMBIENT_RAISE`, which blocks ambient capability elevation.

Each flag has a "LOCKED" version to prevent further modifications, ensuring capability configuration security. Securebits are modifiable via `prctl`, require `CAP_SETPCAP` for changes, and except for `SECBIT_KEEP_CAPS`, are inherited through `execve()`. Securebits enhance security by enabling stricter policies and adherence to the least privilege principle, reducing privilege escalation risks.

---

## capget() and capset()

Programmatic management of capability sets in Linux allows processes (or threads) to query and modify their effective, permitted, and inheritable capability sets. This is done through the system calls capget(2) and capset(2), as well as through the higher-level functions cap_get_proc(3) and cap_set_proc(3) provided by the libcap library. Here is a detail on how they work and the rules governing these modifications:

### Using capget(2) and capset(2)

- `capget(2)`: Allows a process to retrieve its current capability sets. This system call is used to query a thread's effective, permitted, and inheritable capability sets.
- `capset(2)`: Allows a process to modify its capability sets. This includes the effective, permitted, and inheritable capabilities, according to certain detailed security rules.

---

### Rules for Modifying Capability Sets

Modifications to a thread's capability sets are subject to several important security constraints:

1. Modifying the Inheritable Set:

   - If the calling process does not have the CAP_SETPCAP capability, the new inheritable set must be a subset of the combination of the existing inheritable and permitted sets. This means a process cannot assign itself new inheritable capabilities without already having those capabilities in its permitted set or having CAP_SETPCAP.

   - Since Linux 2.6.25, the new inheritable set must also be a subset of the combination of the existing inheritable set and the capability bounding set.

2. Modifying the Permitted Set:

   - The new permitted set must be a subset of the existing permitted set. In other words, a process cannot acquire new permitted capabilities beyond those it already has.

---

### Rules for Modifying Capability Sets (cont)

3. Modifying the Effective Set:

   - The new effective set must be a subset of the new permitted set. This ensures a process cannot activate capabilities as effective if it does not already have them in its permitted set.

4. Modifying the Bounding Set:

   - You can only remove capabilities from the *bounding set* using the `prctl(2) PR_CAPBSET_DROP` operation. Once a capability has been dropped from the bounding set, it cannot be restored to that set.

   - To remove a capability from the bounding set, the process needs to have the `CAP_SETPCAP` capability in its effective set.

---

### More on capabilities

See [this blogpost from Adrian Mouat](https://blog.container-solutions.com/linux-capabilities-in-practice) for more details and examples on capabilities.

---

## cgroups

cgroups (control groups) are a Linux kernel feature that limits, accounts for, and isolates the resource usage (CPU, memory, disk I/O, network, etc.) of a collection of processes.

cgroups are used to:

- Limit the amount of resources a process or a group of processes can use
- Prioritize the allocation of resources to a process or a group of processes
- Account for the usage of resources by a process or a group of processes
- Isolate the usage of resources by a process or a group of processes
- ...

For instance, you can use cgroups to limit the amount of CPU a process can use to 50%, to limit the amount of memory a process can use to 1GB, to prioritize the disk I/O of a process, etc.

---

### cgroups in details

In the following slides, we will only consider the cgroups v2, which is the latest version of cgroups. The v1 is still used, but it is not recommended to use it for new projects.

cgroups v2 is a unified hierarchy, which means that all the cgroups are organized in a single hierarchy. A process can be a member of only one cgroup in the hierarchy. Every thread in a process is a member of the same cgroup as the process.

At the creation of a new process, it is a member of the same cgroup as its parent process.

A process can be moved to another cgroup in the hierarchy, but it does not affect the cgroups of its children processes.

---

### cgroups creation

A cgroup can be created using the `cgroup2` filesystem, which is usually mounted at `/sys/fs/cgroup`:

```shell
$ sudo mkdir /sys/fs/cgroup/freezer
```

This command creates a new cgroup called `freezer`.

To put a process in a cgroup, you can write its PID in the `cgroup.procs` file of the cgroup:

```shell
$ echo $PID > /sys/fs/cgroup/freezer/cgroup.procs
```

To see the cgroups a process is a member of, you can read the `cgroup` file of the process in the `proc` filesystem:

```shell
$ cat /proc/$PID/cgroup
0::/freezer
```

---

### cgroups hierarchy

A cgroup can have sub-cgroups. A sub-cgroup is a cgroup that is a member of another cgroup.

A cgroup can have a set of resources associated with it. These resources can be limited, prioritized, accounted for, and isolated.

A cgroup can have a set of controllers associated with it. A controller is a kernel feature that allows you to manage a specific set of resources.

The hierarchy of cgroups allows you to organize the cgroups in a tree-like structure. The root cgroup is the root of the hierarchy. When a controller is enabled in a cgroup, it is also enabled in all the sub-cgroups of the cgroup.

E.g. if you limit the amount of RAM a cgroup can use to 1GB, all the sub-cgroups of the cgroup will also be limited to 1GB.

To create a sub-cgroup, you can create a new directory in the directory of the parent cgroup:

```shell
$ sudo mkdir /sys/fs/cgroup/freezer/sub
```

---

### cgroups core

The core of cgroups is the `cgroup` filesystem. It is a virtual filesystem that allows you to manage the cgroups.

- Hierarchical organization of cgroups: kernel organizes cgroups in a tree-like structure, where each node in the tree is a cgroup
- Process association: kernel is responsible for associating processes with cgroups, moving processes between cgroups, and inheriting cgroups
- System interface: kernel provides a system interface to manage cgroups, which is usually mounted at `/sys/fs/cgroup`, which allows you to create, delete, and manage cgroups via files and directories in the virtual filesystem

---

### cgroups controllers

A cgroup controller is an entity that allows you to manage a specific set of resources, such as CPU, memory, disk I/O, network, etc., inside the cgroup hierarchy.

- Resources specialization: each controller is specialized in managing a specific set of resources: 
  - `cpu` controller for CPU,
  - `memory` controller for memory,
  - `pid` controller for the number of processes,
  - `cpuset` controller to assign CPUs and memory nodes to a cgroup,
  - `io` controller for disk I/O,
  - ...
- Hierarchical organization: controllers apply management policies to the cgroups in the hierarchy, meaning that the policies are inherited by the sub-cgroups
- Flexibility and granularity: administrators can enable and disable controllers in each cgroup. This lets you apply different management policies to each group of processes

---

### cgroups namespaces

cgroups namespaces allow you to create a new cgroup hierarchy that is isolated from the rest of the system. I.e. the cgroup root is different from the root of the system.

This is useful to isolate the visibility of the cgroups from the rest of the system. For example, you can create a new cgroup namespace for a container, so that the container can only see the cgroups that are associated with it.

#### Example

Let's create a new cgroup namespace and put a process in a cgroup in the new namespace:

```shell
$ sudo mkdir /sys/fs/cgroup/new
$ echo $$ > /sys/fs/cgroup/new/cgroup.procs
$ cat /proc/$$/cgroup
0::/new
$ sudo unshare --cgroup
$ cat /proc/$$/cgroup
0::/
```

---

# From Namespaces to Containers

---

## Automating the Containerization Process with runc

runc is a CLI tool that allows you to create and run containers according to the [OCI specification](https://opencontainers.org/)

Some of the features of runc:

- Support for Linux namespaces
- Security features (cgroups, capabilities, seccomp, etc.)
- Specification of the container runtime configuration format based on the OCI specification

To create a container with runc, you need to create a configuration file that describes the container and a root filesystem that contains the files that the container will use.

The configuration file contains the following information:

- The path to the root filesystem
- The process that will be run in the container
- The environment variables that will be set in the container
- The capabilities that the container will have
- ...

---

## Managing Containers with Containerd

*Containerd* is a container runtime. A container runtime is responsible for managing the complete container lifecycle of its host system, particularly the following responsibilities:

- image transfer and storage
- container execution and supervision
- low-level storage to network attachments and beyond.

It uses *runc* to run containers.

*Containerd* is used by Docker, Kubernetes, etc. and follows the OCI specification, however some alternatives exist such as *CRI-O*.
