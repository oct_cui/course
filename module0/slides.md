---
theme: default
background: https://cover.sli.dev
class: text-center
highlighter: shiki
lineNumbers: false
info: |
  # Containerization and Orchestration Technologies
  A course by [Malik Algelly](https://ch.linkedin.com/in/malik-algelly-60a7081b2) and [Hugo Haldi](https://haldih.github.io/) for the CUI at the University of Geneva.
drawings:
  persist: false
transition: slide-left
title: Containerization and Orchestration Technologies
mdc: true
---

# Containerization and Orchestration Technologies

A course by [Malik Algelly](https://ch.linkedin.com/in/malik-algelly-60a7081b2) and [Hugo Haldi](https://haldih.github.io/) for the CUI at the University of Geneva.

---

## Admin

- Credits: 6
- Course on Tuesday 12h-14h
- Exercises on Tuesday 14h-16h
- TPs every week (depending on the module)
- Evaluation: Project and oral exam
- Need to validate at least 80% of the TPs to present at the oral

---

## Contents

- [Containerization in depth](/module1/)
- [Docker: Automating the Containerization Process](/module2/)
- [Docker compose: First glance to orchestration](/module3/)
- [Docker: Design dockerizable applications](/module3.5/)
- [Kubernetes: Automating the Orchestration Process](/module4/)
- [Kubernetes: Advanced Concepts](/module4.5/)
- [Helm Basics](/module5/)
- [CI/CD with Kubernetes and GitLab CI/CD](/module6/)