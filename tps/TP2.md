---
title: Containerization and Orchestration Tools
subtitle: Docker Basics
author: Hugo Haldi and Malik Algelly
date: Spring 2025
numbersections: true
geometry: margin=2.5cm
---

# Theorical questions

1. Cite 3 advantages of using containers.
2. What is the purpose of the Docker engine?
3. How is built a Docker image? Give technical aspects (layers, ...) and how it is practically implemented? (complete answer)
4. What is the name of the resource used to publish and retrieve Docker images?
5. What happens to the data in a container when the container is stopped? And when it is removed?
6. What technique is used to prevent it?
7. By default, is it possible for a container to reach another running container on the same host? Explain why and how Docker can help.
8. If a container is using the host network and running a service on port 8080, can I run a service on the same port on the host? Why?
9. Same question with a bridge network.

# Practical work

## Run Hello World container

The Docker Hub registry contains a `hello-world` image that tests your ability to run Docker containers. When you run this image, it prints an informational message and exits.

1. Pull the `hello-world` image from the Docker Hub registry:
    
```bash
docker pull hello-world
```

2. Run the `hello-world` image and verify that it prints a message and exits:

```bash
docker run hello-world
```

It should print something like this:

```
Hello from Docker!
This message shows that your installation appears to be working correctly.
```

### Work to return

- A screenshot (or copy-paste) of the output of the `docker run hello-world` command.
- A screenshot of the output of the `docker images` command.
- A brief explanation of what the `pull` and `run` commands do.

## Create our own Hello World image

In the previous step, we used an image that was created by someone else (the Docker team). Now, we will create our own image.

The main steps to create this image are:

1. Choose a base image (e.g. `alpine`)
2. Define the commands to run when the container is started
3. Build the image
4. Run a container based on this image
5. Check that everything works as expected

### Work to return

Fork this repo: [gitlab.unige.ch/oct_cui/helloworld](https://gitlab.unige.ch/oct_cui/helloworld)

- Commit the Dockerfile used to build the image and give the link to the repository
- Push the image to the GitLab registry and provide the link to the image
- Provide a brief explanation of the Dockerfile and the commands used to build and run the image

Note: you have to login to the registry before you can push the image. To do so, enter the following command:

```bash
docker login registry.gitlab.unige.ch
```

You can enter any username, however you have to generate a [Personal Access Token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html) and enter it in the `password` field.

PS: in this case, the registry to use is `registry.gitlab.unige.ch`, however it could be any registry that you want to use.

## Dockerize a simple web application

In this step, we will create a Docker image for a simple NodeJS web application: [gitlab.unige.ch/oct_cui/helloworld-js](https://gitlab.unige.ch/oct_cui/helloworld-js). The application is a simple web server that returns a "Hello World" message.

To get the source code of the application, clone the following repository:

```bash
git clone https://gitlab.unige.ch/oct_cui/helloworld-js.git
```

To run the application *natively*, NodeJS must be installed, and the following commands must be executed:

```bash
cd helloworld-js
npm install # install dependencies
npm start # start the application
```

The application is now running on port 3000. You can check it by opening the following URL in your browser: http://localhost:3000.

FYI: You don't have to install and deploy this application on your PC. These instructions are made to help you creating the Dockerfile.

The goal of this exercise is to create a *Dockerfile* using the previous commands to build and run the application in a container exposing the port 3000.

The main steps to create this image are:

1. Choose a base image (e.g. `node:slim`)
2. Copy the source code of the application in the image (e.g. in `/app`, think to change the working directory)
3. Install the dependencies
4. Define the commands to run when the container is started
5. Expose the port used by the application
6. Build the image, with the tag `helloworld-js:latest`
7. Run a container based on this image
8. If everything works as expected, the application should be accessible at http://localhost:3000

Congratulations, you have dockerized your first application!

### Work to return

- Commit the Dockerfile used to build the image and give the link to the repository
- Push the image to the GitLab registry and provide the link to the image
- Provide a brief explanation of the Dockerfile and the commands used to build and run the image
- A screenshot of the application running in a container

# How to publish images to GitLab registry

First, you have to [create a Personal Access Token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html#create-a-personal-access-token) in your account settings. You have to give the `read_registry` and `write_registry` permissions to the token.

Then, in the project where you want to publish the image, you have to activate the container registry. To do so, go to the project page, **Settings** -> **General** -> **Visibility, project features, permissions** -> **Container registry** and make sure the switch is turned on.

Once the *container registry* is enabled, the element **Container Registry** should appear in the **Deploy** section of the sidebar. Click on it and you will find the commands to push an image to the registry.

The first command is to login to the registry:

```bash
docker login registry.gitlab.unige.ch
```

You can enter any username and the Personal Access Token as the password.

You can then use the given command to tag and push the image to the registry.

To learn more about the GitLab registry, you can read the [official documentation](https://docs.gitlab.com/ee/user/packages/container_registry/).
