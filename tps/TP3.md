---
title: Containerization and Orchestration Tools
subtitle: Advanced Docker
author: Hugo Haldi and Malik Algelly
date: Spring 2025
numbersections: true
geometry: margin=2.5cm
---

# Theorical questions

1. Can a container escape from its isolation? How? (short answer, 2 ways at least)
2. What should we pay attention to when installing a package in a container? (both from a security and a performance point of view)
3. We have an architecture with a backend and a database. How to limit the access of the database to the backend only?

# Practical work

## Multi-stage builds

Docker provides a feature called [multi-stage builds](https://docs.docker.com/develop/develop-images/multistage-build/) that allows to build an image in multiple stages. This feature is useful to reduce the size of the final image by removing the build dependencies.

In this exercise, we will use this feature to build a Docker image for a simple TypeScript web application. The application is a simple web server that returns a "Hello World" message.

First, you have to fork the following repository: [gitlab.unige.ch:oct_cui/helloworld-ts](gitlab.unige.ch:oct_cui/helloworld-ts)

Then, clone the repository to your local machine:

```bash
git clone git@gitlab.unige.ch:<yourusername>/helloworld-ts.git
```

You can follow the instructions in the `README.md` file to build and run the application.

The goal of this exercise is to create a Dockerfile that builds the application and creates a Docker image. The Dockerfile should use multi-stage builds to reduce the size of the final image.

### Steps

1. Fork the repository `helloworld-ts` to your GitLab account.
2. Create a Dockerfile that will contains the multi-stage build. The first stage should build the application and the second stage should copy the build artifacts and run the application.
3. Define the build stage. The build stage should use the `node:slim` image and should copy the `package.json` and `package-lock.json` files to the container. Then, it should install the dependencies using the  `npm install` command.
4. Copy the while files inside the stage and build the code using `npm run build`. The default output directory is `dist/` and the start script is `dist/index.js`.
5. Define the run stage. The run stage should use the `node:alpine` image and should copy the build artifacts from the build stage to the container. Then, it should run the application. To run the application, you need to install *only* the runtime dependencies (without the TypeScript compiler) using `npm install --omit=dev`, with the `package.json` and `package-lock.json` in the current directory, and then run the application using `node dist/index.js`.
6. Build the Docker image and run the container.
7. Push the Docker image to the GitLab registry (see last TP).

### Work to be delivered

- The Dockerfile
- The commands used to build and run the Docker image
- The commands used to push the Docker image to the GitLab registry
- The URL of the Docker image in the GitLab registry
- The URL of the forked repository

## Mini chat application using compose

In this exercise, we will create two Docker containers that will communicate with each other. We won't use Dockerfiles, but we will use the `docker run` command to create the containers, using the `busybox` image.

To do so, we will use the `nc` command, which is a utility to read and write data across network connections, using TCP or UDP protocol.

We want you to do the following steps:

1. Create a network called `chat`
2. Create a container called `server` that will be attached to the `chat` network
3. In this container, run the following command: `nc -l -p 3000` (this command will listen on port 3000)
4. Create a container called `client` that will be attached to the `chat` network
5. In this container, run the following command: `nc server 3000` (this command will connect to the `server` container on port 3000)
6. In the `client` container, type a message and press enter
7. Check that the message is received in the `server` container
8. In the `server` container, type a message and press enter
9. Check that the message is received in the `client` container

### Work to be delivered

- The compose file
- The commands used to create the containers and run the commands
- A screenshow or a text that shows that the messages are received in the `server` and `client` containers
- A brief explanation of what you did

## Persisting data

In this exercise, we want to create a container that will store data in a persistent volume, and when the container is removed, the data must be kept.

To do so, we will use the `busybox` image, and the `docker run` command. First, we want to mount a volume inside the container, in the `/data` directory. Then, we want to create a file in this directory, and write "Hello persistent world" in `/data/persistent.txt`.

Remove the container, and create a new one, mounting the same volume in the same directory. Check that the file `/data/persistent.txt` exists, and contains the text "Hello persistent world".

### Work to be delivered

- The compose file
- The commands used to create the containers and run the commands
- A screenshow or a text that shows that the file `/data/persistent.txt` exists and contains the text "Hello persistent world"
- A brief explanation of what you did
