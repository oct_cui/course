---
title: Containerization and Orchestration Tools
subtitle: Low level containerization
author: Hugo Haldi and Malik Algelly
date: Spring 2025
lang: en
---

- Deposit on Moodle
- Due date: 2025-03-04 12:00

---

# Theorical questions

1. What is a container? What are the main differences comparing to virtual machines? (short answer)
2. What is a namespace? What is its purpose?
3. What is the fundamental difference between a PID namespace and a network namespace? (short answer)
4. What should be done to make two processes in different network namespaces communicate with each other?
5. What should be present in a root filesystem to be used to create a container?
6. What is the purpose of the `runc` command?
7. Is the kernel inside a container the same as the host's kernel? Why?
8. Why sould we also mount the `proc` filesystem while creating a PID namespace?

# The `unshare` command

Create a whole container from scratch using the `unshare` command.

1. Download a pre-built mini root filesystem from the [Alpine Linux](https://alpinelinux.org/downloads/) website. The `minirootfs` is a minimal root filesystem that can be used to create a container. It contains the basic files and directories needed to run a Linux system, such as `/bin`, `/dev`, `/etc`, `/lib`, `/proc`, `/sys`, `/tmp`, `/usr`, `/var`, etc.
2. Use the `unshare` command to create a new namespace for the container. Create the following namespaces:
    - `uts` (hostname)
    - `pid` (processes)
    - `mnt` (mount points)
    - `net` (network)
    - `user` (user and group IDs)
    - `cgroup` (control groups)
3. Use the `chroot` command to change the root directory of the container to the root filesystem
4. Mount the `proc` filesystem in the container

Note: step 2, 3 and 4 can be done in a single command using some options of the `unshare` command (c.f. `man unshare`).

# The `ip netns` command

The goal of this exercise is to make two processes in different network namespaces communicate with each other.

Once the network namespaces and the veth pair are created, try to make the two processes communicate with each other using the `ping` command.

# The `runC` command

Reuse the mini filesystem to create a container using the `runC` command. The `runC` needs both a root filesystem and a configuration file to create a container.

1. Create a configuration (specification) file for the container (c.f. `runc --help`)
2. Look at the `config.json` file generated and try to understand its content
3. Using both the root filesystem and the configuration file, create a container using the `runC` command
