---
title: Containerization and Orchestration Tools
subtitle: Getting started with Kubernetes
author: Hugo Haldi and Malik Algelly
date: Spring 2025
numbersections: true
geometry: margin=2.5cm
---

# Introduction

This practical work will help you to get started with Kubernetes. You will create a Kubernetes cluster using MicroK8s, deploy a few applications, handle persistent storage and networking, and monitor the cluster.

For each task, you will have to write a short report explaining what you did and what each command do. You can use the Markdown syntax to write your report.

# Installing MicroK8s

[MicroK8s](https://microk8s.io/) is a lightweight Kubernetes distribution that runs on Linux. It is a great way to get started with Kubernetes on your local machine or on a small server. Microk8s is a [snap package](https://snapcraft.io/), so you can install it on any Linux distribution that supports snap.

Follow the instructions on the [MicroK8s website](https://microk8s.io/) to install MicroK8s on your machine.

Note: even if Microk8s runs on Linux, it can be installed on Windows and macOS directly. For a more advanced setup, you can see the [alternative installation methods](https://microk8s.io/docs/install-alternatives).

Once you have installed MicroK8s, it should automatically create and start a Kubernetes cluster. You can check the status of the cluster by running the following command:

```bash
microk8s status --wait-ready
```

# Deploying the Kubernetes Dashboard

The Kubernetes Dashboard is a web-based user interface for managing Kubernetes clusters. It allows you to deploy applications, troubleshoot issues, and monitor the cluster.

To deploy the Kubernetes Dashboard, follow the instructions on the [Microk8s dashboard add-on page](https://microk8s.io/docs/addon-dashboard).

Once the dashboard is deployed, you should be able to access it through your web browser on `https://127.0.0.1:10443/` if you are running MicroK8s on your local machine. On Windows or macOS, you should be able to access it through `https://$CONTAINER_IP:10443`.

# Deploying Applications

Now that you have a running Kubernetes cluster and the Kubernetes Dashboard, you can start deploying applications.

## Deploying an Hello World Application (nginx)

The first application you will deploy is a simple web server using the [nginx](https://www.nginx.com/) image.

- First, create a deployment for the nginx application and expose the port 80.
- Then, create a service to expose the deployment.
- Third, create an ingress to expose the service to the outside world, using the default nginx ingress controller.
- Finally, check that the application is running and accessible.

Note: you have to have an ingress controller running in your cluster to expose the service to the outside world. You can use the default nginx ingress controller that comes with MicroK8s, see the [MicroK8s ingress documentation](https://microk8s.io/docs/addon-ingress).

Work to return:

- The yaml files for the deployment, service, and ingress.
- The commands you used to create the deployment, service, and ingress.
- A small report explaining what each command does.

## Configuring and Managing Persistent Storage in Kubernetes

### Objective

Understand and apply the concepts of Persistent Volumes, Persistent Volume Claims, and Storage Classes in a Kubernetes environment by setting up a simulated persistent storage scenario for a web application.

### Instructions

1. Prepare the Storage Class
   - Create a Storage Class named local-storage that will dynamically provision storage using the microk8s.io/hostpath provisioner (see [hostpath documentation](https://microk8s.io/docs/addon-hostpath-storage)).

2. Create a Persistent Volume Claim
   - Define a PVC that requests 1Gi of storage and uses the ReadWriteOnce access mode.

3. Deploy an Application Using the PVC
   - Deploy an Nginx web server that uses the PVC for its web content storage.
   - Mount the volume at /usr/share/nginx/html.

4. Verification
   - Check the status of the PVC to ensure it's bound to a dynamically provisioned PV.

   - Check the pod to ensure it's running and the volume is correctly mounted.

5. Cleanup
   - Remove the pod, PVC, and the Storage Class.

Work to return:

- The yaml files for the Storage Class, Persistent Volume Claim, and Deployment.
- The commands you used to create the Storage Class, Persistent.
- Output of the commands used to check the status of the PVC and the pod.
- Small report explaining what each command does.