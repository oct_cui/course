---
title: Containerization and Orchestration Tools
subtitle: Getting started with Helm
author: Hugo Haldi and Malik Algelly
date: Spring 2025
numbersections: true
geometry: margin=3cm
---

# Introduction

This practical work will help you to get started with Helm. You will install Helm, deploy a hello-world application using Helm, create a hello-world chart, and deploy a more complex application using Helm.

For each task, you will have to write a short report explaining what you did and what each command does. You can use the Markdown syntax to write your report.

## Installing Helm

[Helm](https://helm.sh/) is a package manager for Kubernetes that helps you define, install, and manage applications on your Kubernetes cluster. Helm uses charts to define the structure of an application, including the Kubernetes resources needed to run the application.

If you are using Microk8s, Helm should already be installed, and can be used using `microk8s.helm` command, and aliased to `helm` (e.g. `alias helm='microk8s.helm'` in your `.profile`). If you are using another Kubernetes distribution, you can follow the instructions on the [Helm website](https://helm.sh/docs/intro/install/) to install Helm on your machine.

# Practical Work

## Deploying a Hello-World Application using Helm

The first application you will deploy is a simple web server using the [nginx](https://www.nginx.com/) image. You will use the hello-world chart provided by Helm to deploy the application.

### Adding the Helm example repository

To add the Helm example repository, run the following command:

```bash
helm repo add examples https://helm.github.io/examples
```

### Deploying the hello-world application

Install the hello-world chart using the following command:

```bash
helm install hello-world examples/hello-world
```

### Overriding the default values

The hello-world chart comes with a `values.yaml` file that defines the default values for the chart. You can access the file in the GitHub repository of the chart: [github.com/helm/examples/blob/main/charts/hello-world/values.yaml](https://github.com/helm/examples/blob/main/charts/hello-world/values.yaml).

To override the default values, you can either:

- create a `values.yaml` file with your custom values and pass it to the `helm install` command using the `-f` flag: `helm install hello-world examples/hello-world -f my-values.yaml`
- pass the values directly to the `helm install` command using the `--set` flag: `helm install hello-world examples/hello-world --set key1=value1,key2=value2`. E.g. if you want to change the `image.repository` value, you can run `helm install hello-world examples/hello-world --set image.repository=nginx:alpine`.

Try to override the default `replicaCount` value of the hello-world chart to `2`.

You can simulate the installation of the chart by using the `--dry-run` flag: `helm install hello-world examples/hello-world --dry-run`. This will output the Kubernetes resources that would be created without actually creating them.

In your report, add the generated Kubernetes manifest files and explain what each file does.

## Creating a Hello-World Chart

Now that you have deployed the hello-world application using the hello-world chart, you will create your own chart for a simple web server using the nginx image.

### Creating the chart

Create a new chart called `my-nginx` using the following command:

```bash
helm create my-nginx
```

This will create a new directory called `my-nginx` with the structure of a Helm chart:

```
my-nginx/
|-- charts/
|-- Chart.yaml
|-- templates/
|   |-- _helpers.tpl
|   |-- NOTES.txt
|-- values.yaml
```

The `templates/` directory contains the templates files. When Helm evaluates a chart, it will send all the files in the `templates/` directory through the template rendering engine. The output of the template rendering will be sent to Kubernetes.

The `values.yaml` file contains the default values for the chart. You can override these values when installing the chart using the `--set` flag or by providing a custom `values.yaml` file.

### Customizing the chart

Since we're writing a chart from scratch, remove all the templates files in the `templates/` directory.

#### ConfigMap

Create a ConfigMap to store the nginx configuration `my-nginx/templates/configmap.yaml`. You can put a hello-world HTML page in the ConfigMap (e.g. `<h1>Hello, World!</h1>`).

#### Deployment

Create a Deployment to run the nginx container `my-nginx/templates/deployment.yaml`. Use the nginx image and mount the ConfigMap as a volume.

Hint: the static HTML files should be stored in the `/usr/share/nginx/html` directory in the nginx container.

#### Service

Create a Service to expose the nginx Deployment `my-nginx/templates/service.yaml`.

Hint: nginx listens on port 80.

#### Ingress

Create an Ingress to expose the nginx Service to the outside world `my-nginx/templates/ingress.yaml`.

You can use the `example.com` domain for the Ingress, and you can test your ingress by setting the header `Host: example.com` in your HTTP request.

E.g. using `curl`:

```bash
curl -H "Host: example.com" http://$INGRESS_IP
```

Replace `$INGRESS_IP` with the IP address of your Ingress.

### Deploying the chart

Install the `my-nginx` chart using the following command:

```bash
helm install my-nginx ./my-nginx
```

### Packaging the chart

Package the `my-nginx` chart using the following command:

```bash
helm package my-nginx
```

This will create a `.tgz` file that contains the chart files.

### Work to return

- The Helm package file for the `my-nginx` chart.
- The resulting Kubernetes manifest files for the ConfigMap, Deployment, Service, and Ingress, using `--dry-run` flag.
- A small report explaining what each file does.
