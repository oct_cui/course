---
title: Containerization and Orchestration Tools
subtitle: Deploy a full stack application with Docker Compose
author: Hugo Haldi and Malik Algelly
date: Spring 2025
numbersections: true
geometry: margin=2.5cm
---

# Practical work

We would like to deploy a Nextcloud instance with a PostgreSQL database using Docker Compose. The goal of this exercise is to create a `docker-compose.yml` file that defines the services and the network for the application.

You have to deploy the following services:

- A [PostgreSQL](https://www.postgresql.org/) database
- A [Nextcloud](https://nextcloud.com/) instance
- A [Redis](https://redis.io/) cache
- A [Collabora Online](https://www.collaboraoffice.com/code/) instance for document editing
- A cron job that runs the Nextcloud background jobs
- A [nginx reverse proxy](https://github.com/nginx-proxy/nginx-proxy)
- [Optional] A [Let's Encrypt](https://letsencrypt.org/) container to generate SSL certificates

All the images can be found on [Docker Hub](https://hub.docker.com/).

The services should be connected to a network that is not accessible from the outside. The nginx reverse proxy should be connected to a network that is accessible from the outside. The nginx reverse proxy should also be connected to the services that need to be accessible from the outside.

# Work to be delivered

You have to deliver a `docker-compose.yml` file that defines the services and the network for the application. You also have to deliver a `README.md` file that explains how to deploy the application.

- Compose file that defines the services and the network for the application
- `README.md` file that explains how to deploy the application
- [Optional] Deploy the application on a server and provide the URL (you can use a free service like [Heroku](https://www.heroku.com/))

Note that the Let's Encrypt container is optional and requires a domain name to generate SSL certificates. If you use a free service like Heroku, you can use the domain name provided by Heroku.
